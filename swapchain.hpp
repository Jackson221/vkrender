/*!
 * @file
 *
 * @author Jackson McNeill
 *
 * Provides swapchain-related functions
 */
#pragma once
#include <vector>
#include <stdexcept>

#include <vulkan/vulkan.hpp>
#include "SDL.h"

#include "vkrender/image.hpp"
#include "vkrender/render_state.hpp"

constexpr const int MAX_FRAMES = 2;

namespace vkrender
{


	class swapchain
	{
		private:
			vk::PhysicalDevice p_device;
			vk::Device device;
		public:
			vk::SurfaceKHR surface;
		public: //private: TODO
			SDL_Window* window;
			state& my_state;
			vk::Queue present_queue;
			swapchain(vk::PhysicalDevice p_device,vk::Device device, vk::SurfaceKHR surface, SDL_Window* window, state& my_state, vkrender::device_swapchain_info support_details);

			void recreate();
		public:
			inline swapchain(vk::PhysicalDevice p_device,vk::Device device, vk::SurfaceKHR surface, SDL_Window* window, state& my_state) : 
				swapchain(p_device,device,surface,window,my_state,device_swapchain_info(p_device,surface))
			{
			}
			vk::SurfaceFormatKHR choose_surface_format(std::vector<vk::SurfaceFormatKHR> available_formats);
			vk::PresentModeKHR choose_best_mode(std::vector<vk::PresentModeKHR> available_modes);
			vk::Extent2D choose_best_extent(const vk::SurfaceCapabilitiesKHR capabilities, SDL_Window* window);

			/*!
			 * Tells the swapchain to aquire the next frame.
			 *
			 * If anything, such as window resizing, happens in the meantime, it'll be handled automatically. 
			 */
			void aquire_next_frame();
			/*!
			 * Display the current frame onto the screen.
			 *
			 * If anything, such as window resizing, happens in the meantime, it'll be handled automatically. 
			 */
			void present(vk::Semaphore& signal_semaphores);


			
			vk::SurfaceFormatKHR format;
			vk::UniqueSwapchainKHR vk_swapchain;
			std::vector<vk::Image> swapchain_images;
			std::vector<vk::UniqueImageView> swapchain_image_views;
			vk::Extent2D image_extent;
			vk::Format image_format;



			std::vector<vk::UniqueSemaphore> image_available;

			uint32_t current_frame;
			uint32_t current_image_index;

			vk::Format depth_format;
			image depth_image;

			class ResolutionChange : std::exception
			{
				
			};
		private:
			event::listener<resolution_change_event::interface_t> resolution_change_listener;

			bool resolution_just_changed = false;


	};


}
