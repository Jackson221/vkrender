/*!
 * @file
 *
 * @author Jackson McNeill
 */
#pragma once

#include <vulkan/vulkan.hpp>

#include "vkrender/device.hpp"
#include "vkrender/physical_device.hpp"

#include <mutex> //TODO jobsys
namespace vkrender
{
	inline std::mutex deprecated_on_jobsys_queue_sync_mutex; //TODO jobsys
	//TODO: make public device and p_device?
	class command_pool
	{
		queue_family_indices indices;
		public:
			vk::Device device;
			vk::PhysicalDevice p_device;
			command_pool(vk::PhysicalDevice p_device, vk::Device device, vk::SurfaceKHR surface);

			device_queues queues;
			vk::UniqueCommandPool vk_command_pool;
	};
}
