#include "draw.hpp"

#include "vkrender/physical_device.hpp"

namespace vkrender
{
	frame_drawer::frame_drawer(vk::PhysicalDevice p_device, vk::Device device, swapchain& my_swapchain, framebuffer const & my_framebuffer, std::vector<command_buffer> const & command_buffers) :
		device(device),
		my_swapchain(my_swapchain),
		my_framebuffer(my_framebuffer),
		camera(1),
		command_buffer_by_frame(command_buffers)
	{
		uint32_t num_swapchain_images = my_swapchain.swapchain_images.size();

		for(uint32_t i = 0; i < num_swapchain_images; i++)
		{
			render_finished.push_back(device.createSemaphoreUnique({}));
			drawing.push_back(device.createFenceUnique({vk::FenceCreateFlagBits::eSignaled}));
		}
		auto indices = queue_family_indices(p_device,my_swapchain.surface);

		graphics_queue = device.getQueue(indices.graphics_family.value(),0);

		
		camera.relative_rotate(glm::vec3{0,glm::radians(glm::degrees(90.f)),0});
	}
	void frame_drawer::advance_frame()
	{
		try
		{
			my_swapchain.aquire_next_frame();
		}
		catch (swapchain::ResolutionChange&)
		{
			//TODO	
			advance_frame();
			return;
		}
		device.waitForFences(1,&(drawing[my_swapchain.current_image_index].get()),true,std::numeric_limits<uint64_t>::max());
	}
	void frame_drawer::draw()
	{
		auto current_frame = my_swapchain.current_frame;
		
		auto current_image_index = my_swapchain.current_image_index;

		std::array<vk::PipelineStageFlags,1> wait_stages=  {vk::PipelineStageFlagBits::eColorAttachmentOutput};

		vk::SubmitInfo info = {};
		info.waitSemaphoreCount = 1;
		info.pWaitSemaphores = &my_swapchain.image_available[current_frame].get();
		info.pWaitDstStageMask = wait_stages.data();
		info.commandBufferCount = command_buffer_by_frame[current_image_index].buffers.size();
		info.pCommandBuffers = command_buffer_by_frame[current_image_index].buffers.data(); 
		info.signalSemaphoreCount = 1;
		info.pSignalSemaphores = &(render_finished[current_frame].get());

		device.resetFences({drawing[current_image_index].get()});

		deprecated_on_jobsys_queue_sync_mutex.lock();
		if(graphics_queue.submit(1,&info,drawing[current_image_index].get()) != vk::Result::eSuccess)
		{
			throw std::runtime_error("Failed to submit draw command buffer");
		}
		deprecated_on_jobsys_queue_sync_mutex.unlock();
		try
		{
			my_swapchain.present(render_finished[current_frame].get());
		}
		catch (swapchain::ResolutionChange&)
		{
			//TODO	
			advance_frame();
			draw();
			return;
		}
	}


}
