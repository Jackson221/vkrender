#include "swapchain.hpp"

#include <tuple>

#include <vulkan/vulkan.h>
#include "SDL.h"

#include "vkrender/physical_device.hpp"
#include "vkrender/device_memory.hpp"
#include "algorithm/container.hpp"
#include <stdexcept>

namespace vkrender
{
	vk::SurfaceFormatKHR swapchain::choose_surface_format(std::vector<vk::SurfaceFormatKHR> available_formats)
	{
		for (const auto& available_format : available_formats) 
		{
			if (available_format.format == vk::Format::eB8G8R8A8Unorm && available_format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear) 
			{
				return available_format;
			}
		}
		for (const auto& available_format : available_formats) 
		{
			if (available_format.format == vk::Format::eB8G8R8A8Unorm)
			{
				printf("Not able to aquire SrgNonlinear\n");
				return available_format;
			}
		}
		for (const auto& available_format : available_formats) 
		{
			if (available_format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear) 
			{
				printf("Not able to aquire B8G8R8A8Unorm\n");
				return available_format;
			}
		}
		if (available_formats.size() >= 1)
		{
			printf("We are choosing swapchain by force\n");
			return available_formats[0];
		}
		throw std::runtime_error("Couldn't find a suitable swapchain mode");
	}
	vk::PresentModeKHR swapchain::choose_best_mode(std::vector<vk::PresentModeKHR> available_modes)
	{
		vk::PresentModeKHR output;
		bool found_mode=false;
		for( const auto& mode : available_modes)
		{
			if (mode == vk::PresentModeKHR::eImmediate)
			{
				printf("Swapchain immediate\n");
				return mode;
			}
			if (mode == vk::PresentModeKHR::eMailbox)
			{
				printf("Swapchain mailbox\n");
				found_mode = true;
				output = mode;
			}
		}
		if (!found_mode)
		{
			throw std::runtime_error("Couldn't find a suitable swapchain mode");
		}
		return output;
	}
	vk::Extent2D swapchain::choose_best_extent(const vk::SurfaceCapabilitiesKHR capabilities, SDL_Window* window)
	{
		if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
		{
			return capabilities.currentExtent;
		}
		int width, height;
		SDL_GetWindowSize(window,&width,&height);
		return {static_cast<uint32_t>(width),static_cast<uint32_t>(height)};
	}

	const constexpr uint32_t mip_level = 1;
	swapchain::swapchain(vk::PhysicalDevice p_device,vk::Device device, vk::SurfaceKHR surface, SDL_Window* window, state& my_state, vkrender::device_swapchain_info support_details) : 
		p_device(p_device),
		device(device),
		surface(surface),
		window(window),
		my_state(my_state),
		image_extent(choose_best_extent(support_details.capabilities,window)),
		current_frame(0),
		current_image_index(0),
		//depth image
		depth_format(find_supported_format(p_device,{vk::Format::eD32Sfloat, vk::Format::eD32SfloatS8Uint ,vk::Format::eD24UnormS8Uint},vk::ImageTiling::eOptimal,vk::FormatFeatureFlagBits::eDepthStencilAttachment)),
		depth_image(p_device,device,image_extent.width,image_extent.height,mip_level,depth_format,vk::ImageTiling::eOptimal,vk::ImageUsageFlagBits::eDepthStencilAttachment,vk::MemoryPropertyFlagBits::eDeviceLocal),
		resolution_change_listener(my_state.resolution_change_interface,[this](resolution_t new_res)
		{
			this->recreate();
		}, {}, resolution_change_event::priority_t::swapchain)
	{
		depth_image.generate_image_view(vk::ImageAspectFlagBits::eDepth);

		this->format = choose_surface_format(support_details.formats);
		this->image_format = format.format;
		auto mode = choose_best_mode(support_details.present_modes);


		uint32_t image_count = support_details.capabilities.minImageCount+1;
		if (support_details.capabilities.maxImageCount > 0 && image_count > support_details.capabilities.maxImageCount)
		{
			image_count = support_details.capabilities.maxImageCount;
		}

		vk::SwapchainCreateInfoKHR create_info = {};
		create_info.surface = surface;
		create_info.minImageCount = image_count;
		create_info.imageFormat = format.format;
		create_info.imageColorSpace = format.colorSpace;
		create_info.imageExtent = image_extent;
		create_info.imageArrayLayers = 1;
		create_info.imageUsage = vk::ImageUsageFlagBits::eColorAttachment;

		auto indices = queue_family_indices(p_device,surface);
		uint32_t queue_families[] = {indices.graphics_family.value(),indices.present_family.value()};
		if (indices.graphics_family == indices.present_family)
		{
			create_info.imageSharingMode = vk::SharingMode::eExclusive;
			create_info.queueFamilyIndexCount = 0;
			create_info.pQueueFamilyIndices = nullptr;
		}
		else
		{
			create_info.imageSharingMode = vk::SharingMode::eConcurrent;
			create_info.queueFamilyIndexCount = 2;
			create_info.pQueueFamilyIndices = queue_families;
		}
		create_info.preTransform = support_details.capabilities.currentTransform;
		create_info.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
		create_info.presentMode = mode;
		create_info.clipped = true;
		create_info.oldSwapchain = nullptr;

		this->vk_swapchain = device.createSwapchainKHRUnique(create_info);
		swapchain_images = device.getSwapchainImagesKHR(vk_swapchain.get());

		swapchain_image_views.reserve(swapchain_images.size());
		auto info = vk::ImageViewCreateInfo(vk::ImageViewCreateFlags(),vk::Image(),vk::ImageViewType::e2D,format.format,vk::ComponentMapping(),vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor,0,mip_level,0,1));



		for (auto& image: swapchain_images)
		{
			info.image = image;
			swapchain_image_views.push_back(device.createImageViewUnique(info));

			image_available.push_back(device.createSemaphoreUnique({})); // create a semaphore for each image
		}

		present_queue = device.getQueue(indices.graphics_family.value(),0);
	}
	void swapchain::aquire_next_frame()
	{
		vk::Result result;
		try 
		{
			result = device.acquireNextImageKHR(*vk_swapchain,std::numeric_limits<uint64_t>::max(),image_available[current_frame].get(),nullptr,&current_image_index);
		}
		catch (vk::OutOfDateKHRError& err)
		{
			if (resolution_just_changed)
			{
				throw std::runtime_error("resolution change failed -- tried to recreate swapchain, but swapchain still out of date.");
			}
			resolution_just_changed = true;
			//TODO this needs to be in a different module
			int x,y;
			SDL_GetWindowSize(window,&x,&y);
			my_state.change_resolution({static_cast<uint32_t>(x),static_cast<uint32_t>(y)}); //will trigger a swapchain recreation

			throw ResolutionChange{};
		}
		if (result != vk::Result::eSuccess && result != vk::Result::eSuboptimalKHR)
		{
			throw std::runtime_error("Failed to aquire swapchain image");
		}
		resolution_just_changed = false;
	}

	void swapchain::present(vk::Semaphore& signal_semaphore)
	{
		vk::Result result;
		deprecated_on_jobsys_queue_sync_mutex.lock();
		try
		{
			result = present_queue.presentKHR(vk::PresentInfoKHR{1,&signal_semaphore,1,&(vk_swapchain.get()),&current_image_index});
		}
		catch (vk::OutOfDateKHRError& err)
		{
			if (resolution_just_changed)
			{
				throw std::runtime_error("resolution change failed -- tried to recreate swapchain, but swapchain still out of date.");
			}
			resolution_just_changed = true;
			//TODO this needs to be in a different module
			int x,y;
			SDL_GetWindowSize(window,&x,&y);
			my_state.change_resolution({static_cast<uint32_t>(x),static_cast<uint32_t>(y)}); //will trigger a swapchain recreation

			throw ResolutionChange{};
		}
		if (result != vk::Result::eSuccess && result != vk::Result::eSuboptimalKHR)
		{
			throw std::runtime_error("Failed to present swapchain image");
		}
		deprecated_on_jobsys_queue_sync_mutex.unlock();
		current_frame = (current_frame+1) % MAX_FRAMES;
		resolution_just_changed = false;
	}

	/*!
	 * May invalidate references
	 */
	void swapchain::recreate()
	{
		vk::PhysicalDevice _p_device = p_device;
		vk::Device _device = device;
		vk::SurfaceKHR _surface = surface;
		SDL_Window* _window = window;
		state& _my_state = my_state;

		this->device.waitIdle();
		this->~swapchain();
		new (this) swapchain(_p_device,_device,_surface,_window,_my_state);
	}
}
