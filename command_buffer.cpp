#include "command_buffer.hpp"

#include <algorithm>


namespace vkrender
{

	command_buffer::command_buffer(vk::Device device, const command_pool& pool, const render_pass& pass, const framebuffer& my_framebuffer, viewport_info const & my_viewport_info, std::vector< full_object_pipeline *> pipelines, uint32_t this_swapchain_image, state& _my_state ) :
		device(device),
		pool(&pool),
		pass(&pass),
		my_framebuffer(&my_framebuffer),
		my_viewport_info(&my_viewport_info),
		pipelines(pipelines),
		this_swapchain_image(this_swapchain_image),
		my_state(&_my_state),
		resolution_change_listener(_my_state.resolution_change_interface,[this](resolution_t new_res)
		{
			this->recreate();
		}, {}, resolution_change_event::priority_t::command_buffer)
	{
		buffers = device.allocateCommandBuffers({*pool.vk_command_pool,vk::CommandBufferLevel::ePrimary,1});		
	}
	command_buffer::~command_buffer()
	{
		if (!buffers.empty())
			device.freeCommandBuffers(*pool->vk_command_pool, buffers);
	}
	void command_buffer::start()
	{
		auto& buffer = buffers[0];
		buffer.begin({vk::CommandBufferUsageFlagBits::eOneTimeSubmit}); //TODO: Make this optional, as some renderers may re-use the buffer
	}
	void command_buffer::end(std::vector< full_object_pipeline *> pipelines)
	{
		auto& buffer = buffers[0];

		std::array<vk::ClearValue, 2> clear_values;
		clear_values[0].color = vk::ClearColorValue{std::array<float,4>{0,0,0,1.f}}; //std::array<float,4>{102.f/256.f,141.f/256.f,218.f/256.f,1.f}}; //TODO customization point
		clear_values[1].depthStencil = vk::ClearDepthStencilValue{1.f,0};

		vk::RenderPassBeginInfo render_pass_info = {};
		render_pass_info.renderPass = *(pass->pass);
		render_pass_info.framebuffer = *(my_framebuffer->vk_framebuffer);
		render_pass_info.renderArea.extent = my_viewport_info->get_extent();
		render_pass_info.renderArea.offset = my_viewport_info->get_offset();
		render_pass_info.clearValueCount = clear_values.size();
		render_pass_info.pClearValues = clear_values.data();
		buffer.beginRenderPass(&render_pass_info,vk::SubpassContents::eInline);

		for (auto full_pipeline_obj : pipelines)
		{
			buffer.bindPipeline(vk::PipelineBindPoint::eGraphics,full_pipeline_obj->pipeline_obj.vk_pipeline.get());

			full_pipeline_obj->descriptor.bind_objects(*(full_pipeline_obj->pipeline_obj.pipeline_layout), buffer, this_swapchain_image);
		}


		/*
		for (size_t pipeline_iterator = 0; pipeline_iterator < my_pipeline.size(); pipeline_iterator++)
		{
			buffer.bindPipeline(vk::PipelineBindPoint::eGraphics,*(my_pipeline[pipeline_iterator]->vk_pipeline));

			const std::vector<vk::DeviceSize> offsets = {0};
			size_t i =0;
			for (size_t object_id : pipeline_object_ids_ref[pipeline_iterator])
			{
				auto & geo_buf = components_ref.object_geometry_buffers[object_id];
				auto & descriptor = components_ref.object_descriptors_for_swapchain_image[object_id];


				buffer.bindVertexBuffers(0,1,&(geo_buf.vertex_buffer.buffer.get()),offsets.data());
				buffer.bindIndexBuffer(geo_buf.index_buffer.buffer.get(),0,vk::IndexType::eUint32);
				buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics,my_pipeline[pipeline_iterator]->pipeline_layout.get(),0,1,&descriptor[this_swapchain_image].descriptor_set,0,nullptr);
				buffer.drawIndexed(geo_buf.indices,1,0,0,0); 

				i++;
			}
		}
		*/


		buffer.endRenderPass();
		buffer.end();
	}
	void command_buffer::reset()
	{
		buffers[0].reset(vk::CommandBufferResetFlagBits::eReleaseResources);//vk::CommandBufferResetFlags{});
	}
	command_buffer::command_buffer(command_buffer&& other) :
		device(std::move(other.device)),
		pool(std::move(other.pool)),
		pass(std::move(other.pass)),
		my_framebuffer(std::move(other.my_framebuffer)),
		my_viewport_info(std::move(other.my_viewport_info)),
		pipelines(std::move(other.pipelines)),
		this_swapchain_image(std::move(other.this_swapchain_image)),
		my_state(other.my_state),
		resolution_change_listener(my_state->resolution_change_interface,[this](resolution_t new_res)
		{
			this->recreate();
		}, {}, resolution_change_event::priority_t::command_buffer),
		buffers(std::move(other.buffers))
	{
		other.resolution_change_listener.release();
	}
	command_buffer & command_buffer::operator=(command_buffer&& other)
	{
		device = std::move(other.device);
		pool = std::move(other.pool);
		pass = std::move(other.pass);
		my_framebuffer = std::move(other.my_framebuffer);
		my_viewport_info = std::move(other.my_viewport_info);
		pipelines = std::move(other.pipelines);
		this_swapchain_image = std::move(other.this_swapchain_image);
		my_state = other.my_state;
		buffers = std::move(other.buffers);

		other.resolution_change_listener.release();

		return *this;
	}
	void command_buffer::recreate()
	{
		vk::Device _device = device;
		command_pool const * _pool = pool;
		render_pass const * _pass = pass;
		framebuffer const * _my_framebuffer = my_framebuffer;
		viewport_info const * _my_viewport_info = my_viewport_info;
		auto _pipelines = pipelines;
		auto _this_swapchain_image = this_swapchain_image;
		state* _my_state = my_state;

		this->device.waitIdle();
		this->~command_buffer();

		new (this) command_buffer(_device,*_pool,*_pass,*_my_framebuffer,*_my_viewport_info,_pipelines,_this_swapchain_image,*_my_state);
		//don't leave invalid command buffers, start and end them with old data.
		start();
		end(_pipelines);
	}
}
