/*!
 * @file
 *
 * @author Jackson McNeill
 *
 * Some utilities for Vulkan shaders
 */
#pragma once

#include <string>
#include <vulkan/vulkan.hpp>

#include "external/glm.hpp"

namespace vkrender
{
	vk::UniqueShaderModule create_shader(vk::Device device, std::string code);	
	
}
