/*!
 * @file
 *
 * @author Jackson McNeill
 *
 * Provides a texture class that can be loaded from a file.
 */
#pragma once

#include <optional>

#include <vulkan/vulkan.hpp>
#include "SDL.h"

#include "vkrender/command_pool.hpp"
#include "vkrender/image.hpp"

namespace vkrender
{

	
	class texture
	{
		public:
			enum class filter
			{
				nearest,
				linear
			};
		private:
			struct initialization_data
			{
				initialization_data(const std::string& path);
				initialization_data(SDL_Surface* surface_unknown_format, bool& free);

				void init_step_1(SDL_Surface* surface_unknown_format, bool& free);
				SDL_Surface* surface;	
				uint32_t tex_width;
				uint32_t tex_height;
				uint32_t mip_levels;
			};
			texture(command_pool const & my_command_pool, initialization_data ID, filter image_filter, bool normalized_coords, bool const & free); //const & to avoid function args order fiasco
		public:
			inline texture(command_pool const & my_command_pool, std::string path, filter image_filter = filter::linear, bool normalized_coords = true) :
				texture(my_command_pool,initialization_data(path), image_filter, normalized_coords,true)
			{
			}
			//NOTE: For now, the passed SDL_Surface will be freed.
			inline texture(command_pool const & my_command_pool, SDL_Surface* surface, filter image_filter = filter::linear, bool normalized_coords = true, bool free=true) :
				texture(my_command_pool,initialization_data(surface,free), image_filter, normalized_coords,free)
			{
			}
			inline texture(texture&& other) :
				my_image(std::move(other.my_image)),
				sampler(std::move(other.sampler))
			{
			}

			image my_image;
			vk::UniqueSampler sampler;
	};

}
