/*!
 * Vertex struct
 */
#pragma once

#include <vulkan/vulkan.hpp>

#include "external/glm.hpp"

namespace vkrender
{
	struct vertex
	{
		glm::vec3 pos;
		glm::vec3 color;
		glm::vec2 texture_pos;
		glm::vec3 normal;
		inline static const vk::VertexInputBindingDescription get_binding_description()
		{
			return {0,sizeof(vertex),vk::VertexInputRate::eVertex};
		}
		inline static const std::array<vk::VertexInputAttributeDescription,4> get_attribute_descriptions()
		{
			//TODO: Automate.
			std::array<vk::VertexInputAttributeDescription,4> output;
			output[0] = vk::VertexInputAttributeDescription
			{
				0, //location
				0, //binding
				vk::Format::eR32G32B32Sfloat,
				offsetof(vertex,pos),
			};
			output[1] = vk::VertexInputAttributeDescription
			{
				1, //location
				0, //binding
				vk::Format::eR32G32B32Sfloat,
				offsetof(vertex,color),
			};

			output[2] = vk::VertexInputAttributeDescription
			{
				2, //location
				0, //binding
				vk::Format::eR32G32Sfloat,
				offsetof(vertex,texture_pos)
			};
			
			output[3] = vk::VertexInputAttributeDescription
			{
				3, //location
				0, //binding
				vk::Format::eR32G32B32Sfloat,
				offsetof(vertex,normal)
			};
			return output;
		}
		inline bool operator==(vertex& other)
		{
			return (pos==other.pos && color == other.color && texture_pos == other.texture_pos && normal == other.normal);
		}
	};

}
