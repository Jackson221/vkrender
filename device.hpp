/*!
 * @file
 *
 * @author Jackson McNeill
 *
 * Creating Vulkan devices & utilities for them
 */
#pragma once

#include <vulkan/vulkan.hpp>

#include "vkrender/physical_device.hpp"


namespace vkrender
{
	vk::UniqueDevice create_device(vk::PhysicalDevice& p_device, vk::SurfaceKHR& surface,vk::PhysicalDeviceFeatures features,std::vector<const char*> layers,std::vector<const char*> extensions);
	struct device_queues
	{
		device_queues(vk::Device device, queue_family_indices indices);
		vk::Queue graphics_queue;
		vk::Queue present_queue;
		vk::Queue compute_queue;
	};

}
