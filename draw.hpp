/*!
 * Draw a frame
 */
#pragma once 

#include <vulkan/vulkan.hpp>

#include "vkrender/swapchain.hpp"
#include "vkrender/command_buffer.hpp"
#include "vkrender/camera/camera.hpp"

namespace vkrender
{
	class frame_drawer
	{
		public: //TODO
		vk::Device device;
		vk::Queue graphics_queue;
		
		std::vector<vk::UniqueSemaphore> render_finished;
		std::vector<vk::UniqueFence> drawing;

		swapchain& my_swapchain;
		framebuffer const & my_framebuffer;

		std::vector<command_buffer> const & command_buffer_by_frame;

		public:
			frame_drawer(vk::PhysicalDevice p_device, vk::Device device, swapchain& my_swapchain, framebuffer const & my_framebuffer, std::vector<command_buffer> const & command_buffers);
		
			void advance_frame();
			void draw();
			camera::camera camera;

	};

}
