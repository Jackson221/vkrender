/*!
 *	Rendering command buffer
 */
#pragma once

#include <vector>

#include <vulkan/vulkan.hpp>
#include <vulkan/vulkan_core.h>

#include "vkrender/command_pool.hpp"
#include "vkrender/render_pass.hpp"
#include "vkrender/framebuffer.hpp"
#include "vkrender/pipeline.hpp"
#include "vkrender/object.hpp"
#include "vkrender/render_state.hpp"
#include "vkrender/viewport_info.hpp"
#include "vkrender/full_object_pipeline.hpp"


namespace vkrender
{
	struct full_object_pipeline
	{
		full_object_pipeline_descriptor descriptor;

		pipeline pipeline_obj;
	};
	class command_buffer
	{
		vk::Device device;
		command_pool const * pool;
		render_pass const * pass;
		framebuffer const * my_framebuffer;
		viewport_info const * my_viewport_info;
		std::vector< full_object_pipeline *> pipelines;
		uint32_t this_swapchain_image;
		state* my_state;

		event::listener<resolution_change_event::interface_t> resolution_change_listener;

		void recreate();
		public:
			command_buffer(vk::Device device, const command_pool& pool, const render_pass& pass, const framebuffer& my_framebuffer, viewport_info const & my_viewport_info, std::vector< full_object_pipeline *> pipelines, uint32_t this_swapchain_image, state& _my_state );
			~command_buffer();

			void start();
			void end(std::vector< full_object_pipeline *> pipelines);

			void reset();
			command_buffer(command_buffer&& other);
			command_buffer & operator=(command_buffer && other);
			command_buffer(const command_buffer&) = delete;

			std::vector<vk::CommandBuffer> buffers;
	};

}
