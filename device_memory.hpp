/*!
 * @file
 *
 * Utilities to manage device memory
 */
#pragma once 

#include <exception>
#include <stdexcept>
#include <string>

#include <vulkan/vulkan.hpp>

#include "vkrender/single_time_commands.hpp"
#include "vkrender/command_pool.hpp"
#include "algorithm/type_traits.hpp"
#include "algorithm/misc.hpp"


namespace vkrender
{
	class MemoryTypeNotFound : public std::exception
	{
		std::string original; 
		const char* msg;
		public:
			inline MemoryTypeNotFound(uint32_t type)
			{
				original = (std::string("Memory of type ")+std::to_string(type)+std::string(" not found."));
				msg = original.c_str();
			}
			virtual const char* what() const noexcept override
			{return msg;}
	};

	inline uint32_t find_memory_type(vk::PhysicalDeviceMemoryProperties properties, uint32_t type_filter,vk::MemoryPropertyFlags flags)
	{
		for (uint32_t i = 0; i < properties.memoryTypeCount; i++)
		{
			if (
				(type_filter & (uint32_t(1) << i)) && 
				((properties.memoryTypes[i].propertyFlags & flags) == flags)
			)
			{
				return i;
			}
		}
		throw MemoryTypeNotFound(type_filter);
	}
	inline vk::Format find_supported_format(vk::PhysicalDevice p_device, const std::vector<vk::Format>& candidates, vk::ImageTiling tiling, vk::FormatFeatureFlags features) 
	{
		for (auto& candidate : candidates)
		{
			auto props = p_device.getFormatProperties(candidate);
			if ((props.optimalTilingFeatures & features) == features && tiling == vk::ImageTiling::eOptimal)
			{
				return candidate;
			}
			if((props.linearTilingFeatures & features) == features && tiling == vk::ImageTiling::eLinear)
			{
				return candidate;
			}
		}
		throw std::runtime_error("find_supported_format: couldn't find supported format");
	}



	template<typename T>
	constexpr std::enable_if_t<std::is_same_v<T,vk::BufferUsageFlagBits>,unsigned int> vk_flags_proxy(T flags)
	{
		return static_cast<unsigned int>(flags);
	}
	template<typename T1, typename ... T>
	constexpr std::enable_if_t<std::is_same_v<T1,vk::BufferUsageFlagBits>,unsigned int> vk_flags_proxy(T1 flags, T ... more_flags)
	{
		return vk_flags_proxy(flags) | vk_flags_proxy(more_flags...);
	}
	template<typename out_type>
	constexpr out_type vk_flags_deproxy(unsigned int proxied_flags)
	{
		return static_cast<out_type>(proxied_flags);
	}
	

	template<unsigned int usage>
	class device_buffer
	{
		vk::PhysicalDevice p_device;
		vk::Device device;
		public:
			static constexpr bool writable_directly = algorithm::contains_any_flag(usage,
					vk_flags_proxy(vk::BufferUsageFlagBits::eTransferSrc),
					vk_flags_proxy(vk::BufferUsageFlagBits::eUniformBuffer)
			);
				/*((usage & vk_flags_proxy(vk::BufferUsageFlagBits::eTransferSrc)) == vk_flags_proxy(vk::BufferUsageFlagBits::eTransferSrc)) ||
				((usage & vk_flags_proxy(vk::BufferUsageFlagBits::eUniformBuffer)) == vk_flags_proxy(vk::BufferUsageFlagBits::eUniformBuffer))
				;*/

			device_buffer(vk::PhysicalDevice p_device, vk::Device device, vk::DeviceSize size, vk::MemoryPropertyFlags properties) : 
				p_device(p_device),
				device(device),
				size(size),
				buffer(device.createBufferUnique({vk::BufferCreateFlags(),size,vk_flags_deproxy<vk::BufferUsageFlags>(usage),vk::SharingMode::eExclusive}))
			{
				auto memory_requirements = device.getBufferMemoryRequirements(buffer.get());
				memory = device.allocateMemoryUnique({memory_requirements.size,find_memory_type(p_device.getMemoryProperties(),memory_requirements.memoryTypeBits,properties)});
				device.bindBufferMemory(buffer.get(),memory.get(),0);
			}

			

			void* get_mapped_memory()
			{
				return device.mapMemory(memory.get(),0,size);
			}
			void destroy_mapped_memory()
			{
				device.unmapMemory(memory.get());
			}
			template<class buffer_type>
			void copy_buffer_to(use_single_time_command_buffer_with const & objs, buffer_type& other)
			{
				single_time_vulkan_command(device, objs.pool, objs.queue, [this,&other](vk::CommandBuffer cmd_buffer)
				{	
					cmd_buffer.copyBuffer(this->buffer.get(), other.buffer.get(),vk::BufferCopy{0,0,std::min(other.size,this->size)});
				});
			}

			/*!
			 * This will transfer data stored in an STL-style sequence container or C-style array into a buffer.
			 *
			 * If the buffer usage flag "eTransferSrc" is not present, a proxy buffer will be used automatically. 
			 */
			template<algorithm::not_has_size_member_fn T>
			void transfer_c_data_into(use_single_time_command_buffer_with const & objs, T data, uint32_t data_size)
			{
				if constexpr( writable_directly)
				{
					transfer_c_data_into(data,data_size);	
				}
				else
				{
					_transfer_c_data_into_with_proxy_buffer(objs,data,data_size);
				}
			}

			template<algorithm::not_has_size_member_fn T>
			void transfer_c_data_into(T data, uint32_t data_size) 
			{
				if (size < data_size)
				{
					throw std::runtime_error("device_buffer too small to store data");
				}
				void* memory_host_accessable = get_mapped_memory();
				memcpy(memory_host_accessable, reinterpret_cast<void*>(data), data_size);
				destroy_mapped_memory();
			}


			template<algorithm::has_size_member_fn T>
			void transfer_c_data_into(use_single_time_command_buffer_with const & objs, T data)
			{
				transfer_c_data_into(objs,data.data(),data.size()*sizeof(typename T::value_type));
			}

			template<algorithm::has_size_member_fn T>
			void transfer_c_data_into(T data)
			{
				transfer_c_data_into(data.data(),data.size()*sizeof(typename T::value_type));
			}



			vk::DeviceSize size;


			vk::UniqueDeviceMemory memory;
			vk::UniqueBuffer buffer;


		private:

			template<typename T>
			void _transfer_c_data_into_with_proxy_buffer(use_single_time_command_buffer_with const & objs, T data, uint32_t data_size)
			{
				if(data_size > size)
				{
					throw std::runtime_error("device_buffer too small to store data");
				}

				auto proxy_buffer = device_buffer<vk_flags_proxy(vk::BufferUsageFlagBits::eTransferSrc)> (p_device,device,data_size, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
				proxy_buffer.transfer_c_data_into(objs,data,data_size);

				proxy_buffer.copy_buffer_to(objs,*this); //a reference is needed as opposed to a pointer, so "this" is dereferenced
			}
	};


	template<class a> struct is_device_buffer : std::false_type {};
	template<auto a> struct is_device_buffer<device_buffer<a>> : std::true_type {};


}
