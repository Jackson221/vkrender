/*!
 * @file
 *
 * @author Jackson McNeill
 *
 * Contains buffer containers for the graphics pipeline.
 */
#pragma once

#include <vulkan/vulkan.hpp>

#include "vkrender/vertex.hpp"
#include "vkrender/command_pool.hpp"
#include "vkrender/model.hpp"
#include "vkrender/descriptor.hpp"

namespace vkrender
{
	class geometry_buffers
	{
		public:
			geometry_buffers(const command_pool& pool, const std::vector<vertex>& vertices_cont, const std::vector<uint32_t>& indices_cont);

			inline geometry_buffers(const command_pool& pool, const model& mymodel) :
				geometry_buffers(pool,mymodel.vertices,mymodel.indices)
			{
			}

			void update_buffer(command_pool const & pool, std::vector<vertex> const & vertices_cont);
			void update_buffer(command_pool const & pool, std::vector<uint32_t> const & indices_cont);

			device_buffer<vk_flags_proxy(vk::BufferUsageFlagBits::eTransferDst , vk::BufferUsageFlagBits::eVertexBuffer)> vertex_buffer;
			device_buffer<vk_flags_proxy(vk::BufferUsageFlagBits::eTransferDst , vk::BufferUsageFlagBits::eIndexBuffer)> index_buffer;

			size_t indices;
	};
}
