#include "physical_device.hpp"

#include "logger/logger.hpp"


namespace vkrender
{
	std::vector<const char*> get_device_unsupported_extensions(vk::PhysicalDevice& device, std::vector<const char*> extensions)
	{
		std::vector<const char*> result; 
		auto supported_extensions = device.enumerateDeviceExtensionProperties();
		for (const char* extension : extensions)
		{
			bool supported = false;
			for (vk::ExtensionProperties& checked_extension : supported_extensions)
			{
				if (strcmp(checked_extension.extensionName , extension))
				{
					supported = true;
				}
			}
			if (!supported)
			{
				result.push_back(extension);
			}
		}
		return result;
	}
	queue_family_indices::queue_family_indices(vk::PhysicalDevice& device, vk::SurfaceKHR& surface)
	{
		auto queue_families = device.getQueueFamilyProperties();
		uint32_t i =0;
		for (const auto& queue_family : queue_families)
		{
			if(queue_family.queueCount > 0)
			{
				if (queue_family.queueFlags & vk::QueueFlagBits::eGraphics)
				{
					graphics_family = {i};
				}
				vk::Bool32 present_support = device.getSurfaceSupportKHR(i,surface);
				if (present_support)
				{
					present_family = {i};
				}
				if (queue_family.queueFlags & vk::QueueFlagBits::eCompute)
				{
					compute_family = {i};
				}

				if (is_complete())
				{
					break;
				}
			}
			i++;
		}
	}
	bool queue_family_indices::is_complete()
	{
		return graphics_family.has_value() && present_family.has_value() && compute_family.has_value();
	}
	std::set<uint32_t> queue_family_indices::get_unique_queue_families()
	{
		std::set<uint32_t> output;
#define insert_if_has_value(x) if(x.has_value()) {output.insert(x.value());}
		insert_if_has_value(graphics_family);
		insert_if_has_value(present_family);
		insert_if_has_value(compute_family);
		return output;
	}
	device_swapchain_info::device_swapchain_info(vk::PhysicalDevice& device, vk::SurfaceKHR& surface)
	{
		capabilities = device.getSurfaceCapabilitiesKHR(surface);	
		formats = device.getSurfaceFormatsKHR(surface);
		present_modes = device.getSurfacePresentModesKHR(surface);
	}

}
