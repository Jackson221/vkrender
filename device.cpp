#include "device.hpp"

#include "algorithm/container.hpp"

namespace vkrender
{
	vk::UniqueDevice create_device(vk::PhysicalDevice& p_device, vk::SurfaceKHR& surface,vk::PhysicalDeviceFeatures features,std::vector<const char*> layers,std::vector<const char*> extensions)
	{
		std::vector<vk::DeviceQueueCreateInfo> queue_create_infos;

		auto indices = queue_family_indices(p_device,surface);
		float queue_priority = 1.f;
		auto unique_queue_families = indices.get_unique_queue_families();
		for (auto family : unique_queue_families)
		{
			queue_create_infos.push_back({
				vk::DeviceQueueCreateFlags(),
				family,
				1,
				&queue_priority,
			});
		}
		vk::DeviceCreateInfo create_info = {};
		create_info.queueCreateInfoCount = queue_create_infos.size();
		create_info.pQueueCreateInfos = queue_create_infos.data();

		
		create_info.enabledLayerCount = layers.size();
		create_info.ppEnabledLayerNames = layers.data();

		create_info.enabledExtensionCount = extensions.size();
		create_info.ppEnabledExtensionNames = extensions.data();

		create_info.pEnabledFeatures = &features;

		return p_device.createDeviceUnique(create_info);
	}

	device_queues::device_queues(vk::Device device, queue_family_indices indices)
	{
		this->graphics_queue = device.getQueue(indices.graphics_family.value(),0);
		this->present_queue = device.getQueue(indices.present_family.value(),0);
		this->compute_queue = device.getQueue(indices.compute_family.value(),0);
	}


}
