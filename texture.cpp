#include "texture.hpp"

#include <stdexcept>
#include <cstring>
#include <cmath>

#include "SDL_image.h"
#include "SDL.h" //TODO android

#include "vkrender/device_memory.hpp"
#include "filesystem/filesystem.hpp"


#include "logger/logger.hpp"

constexpr char p_path_msg[] = "texture path: %s%";
namespace vkrender
{


	texture::initialization_data::initialization_data(const std::string& path)
	{
		logger::console_log<p_path_msg>(path); //TODO android

		SDL_Surface* surface_unknown_format = IMG_Load(path.c_str()); 
		if (!surface_unknown_format)
		{
			throw std::runtime_error(std::string("failed to load image error: ")+IMG_GetError()); 
		}

		bool a = true;
		init_step_1(surface_unknown_format,a);
	}
	texture::initialization_data::initialization_data(SDL_Surface* surface_unknown_format, bool& free)
	{
		init_step_1(surface_unknown_format,free);
	}
	void texture::initialization_data::init_step_1(SDL_Surface* surface_unknown_format, bool& free)
	{
		if (surface_unknown_format->format->format == SDL_PIXELFORMAT_ABGR8888)
		{
			surface = surface_unknown_format;
		}
		else
		{
			surface = SDL_ConvertSurfaceFormat(surface_unknown_format,SDL_PIXELFORMAT_ABGR8888,0);
			if (free)
				SDL_FreeSurface(surface_unknown_format);
			free = true;
			if (!surface)
			{
				throw std::runtime_error(std::string("failed to convert image error: ")+SDL_GetError());
			}
		}
		tex_width = static_cast<uint32_t>(surface->w);
		tex_height = static_cast<uint32_t>(surface->h);
		mip_levels = std::floor(std::log2(std::max(tex_width,tex_height)))+1;
	}
	texture::texture(command_pool const & my_command_pool, texture::initialization_data ID, filter image_filter, bool normalized_coords, bool const & free) : 
		my_image(my_command_pool.p_device,my_command_pool.device,ID.tex_width,ID.tex_height,ID.mip_levels,vk::Format::eR8G8B8A8Unorm,vk::ImageTiling::eOptimal,vk::ImageUsageFlagBits::eTransferSrc | vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled, vk::MemoryPropertyFlagBits::eDeviceLocal)
	{
		vk::DeviceSize image_size = ID.surface->format->BytesPerPixel * ID.tex_width * ID.tex_height;


		my_image.transition_layout(my_command_pool,my_command_pool.queues.graphics_queue,vk::ImageLayout::eTransferDstOptimal);
		my_image.transfer_c_data_into(use_single_time_command_buffer_with{my_command_pool,my_command_pool.queues.graphics_queue},ID.surface->pixels,image_size); //TODO
		my_image.generate_mipmaps(my_command_pool,my_command_pool.queues.graphics_queue);
		my_image.generate_image_view();
		if (free)
			SDL_FreeSurface(ID.surface);
		

		//create image sampler
		
		vk::SamplerCreateInfo info = {};

		vk::Filter vkfilter;

		switch(image_filter)
		{
			case filter::nearest:
				vkfilter = vk::Filter::eNearest;
				break;
			case filter::linear:
			default:
				vkfilter = vk::Filter::eLinear;
				break;
		}

		info.magFilter = vkfilter;
		info.minFilter = vkfilter;

		auto address_mode = normalized_coords? vk::SamplerAddressMode::eRepeat : vk::SamplerAddressMode::eClampToEdge;

		info.addressModeU = address_mode;
		info.addressModeV = address_mode;
		info.addressModeW = address_mode;

		info.anisotropyEnable = normalized_coords;
		info.maxAnisotropy = 16;

		info.borderColor = vk::BorderColor::eIntOpaqueBlack;

		info.unnormalizedCoordinates = !normalized_coords;

		info.compareEnable = false;
		info.compareOp = vk::CompareOp::eAlways;

		info.mipmapMode = normalized_coords? vk::SamplerMipmapMode::eLinear : vk::SamplerMipmapMode::eNearest;
		info.mipLodBias = 0.f;
		info.minLod = 0.f;
		info.maxLod = normalized_coords? ID.mip_levels : 0.f;

		sampler = my_command_pool.device.createSamplerUnique(info);

	}
}
