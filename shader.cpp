#include "shader.hpp"

#include <stdexcept>
#include <array>

namespace vkrender
{
	vk::UniqueShaderModule create_shader(vk::Device device, std::string code)
	{
		if (code.size() % 4 != 0)
		{
			throw std::runtime_error(std::string("shader code not in 4-byte blocks - size is ")+std::to_string(code.size()));
		}
		return device.createShaderModuleUnique(vk::ShaderModuleCreateInfo{vk::ShaderModuleCreateFlags(),code.size(),reinterpret_cast<uint32_t*>(code.data())});
	}
	
}
