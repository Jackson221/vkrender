#include "camera.hpp"

#include "algorithm/glm.hpp"


namespace vkrender
{
	namespace camera
	{
		camera::camera(unsigned int num_viewports)
		{
			viewports.push_back(viewport{glm::vec3(0,0,0)});
		}
		void camera::relative_push(glm::vec3 push)
		{
			for(auto& viewport : viewports)
			{
				//TODO
				viewport.position += strafe*push.x;
				viewport.position += glm::cross(forward,strafe)*push.y;
				viewport.position += forward*push.z;
			}
		}
		glm::vec3 camera::get_vec_relative_to_camera(glm::vec3 in)
		{
			return 
				strafe*in.x+
				glm::cross(forward,strafe)*in.y +
				forward*in.z;
		}
		void camera::set_position(glm::vec3 position)
		{
			for (auto& viewport : viewports) //TODO: incorportate eye seperation
			{
				viewport.position = position;
			}
		}
		void camera::relative_rotate(glm::vec3 key)
		{
			set_angle(glm::normalize(get_angle()*algorithm::get_key_quat(key)));
		}
		void camera::set_angle(glm::quat new_angle)
		{
			angle = new_angle;
			auto rotation_matrix = glm::inverse(glm::mat4(angle));
			forward = glm::vec3(rotation_matrix[0][2], rotation_matrix[1][2],rotation_matrix[2][2]);
			strafe = glm::vec3(rotation_matrix[0][0],rotation_matrix[1][0],rotation_matrix[2][0]);
		}
		

	}
}
