/*!
 * @file
 *
 * @author Jackson McNeill
 *
 * Provides a camera for use in VR and non-VR applications
 */
#pragma once 

#include <vector>
#include "external/glm.hpp"

namespace vkrender
{
	namespace camera
	{
		struct viewport
		{
			glm::vec3 position;
		};
		class camera
		{
			glm::vec3 forward = glm::vec3(0.0f, 0.0f, +1.0f);
			glm::vec3 strafe = glm::vec3(1.0f, 0.0f,  0.0f);
			glm::quat angle = glm::normalize(glm::quat(glm::vec3(0,0,0)));

			/*!
			 * Desktop apps will have a single viewport, but VR apps will have 2
			 */
			std::vector<viewport> viewports;
			public:
				camera(unsigned int num_viewports);
				void relative_push(glm::vec3 push);
				glm::vec3 get_vec_relative_to_camera(glm::vec3 in);
				void set_position(glm::vec3 position);
				void relative_rotate(glm::vec3 key);

				

				inline glm::vec3 get_position(unsigned int viewport)
				{
					return viewports[viewport].position;
				}
				inline glm::quat get_angle()
				{
					return angle;
				}

				void set_angle(glm::quat new_angle);


		};

	}

}
