/*!
 * Import models
 */
#pragma once

#include <string>
#include <vector>

#include "external/glm.hpp"
#include "vkrender/vertex.hpp"

namespace vkrender
{
	
	class model
	{
		public:
			model(std::string path);
			model(std::vector<uint32_t> indices, std::vector<vertex> vertices);

			std::vector<uint32_t> indices;
			std::vector<vertex> vertices;

	};


}
