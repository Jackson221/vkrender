#pragma once

#include <functional>

#include <vulkan/vulkan.hpp>

namespace vkrender
{
	/*!
	 * This describes a full pipeline: it describes all shaders used,
	 * the VkPipeline attributes, and it describes how to bind the objects
	 * rendered by the pipeline to the command buffer.
	 */
	struct full_object_pipeline_descriptor
	{
		//TODO: More complex shader descriptions
		std::string vertex_shader;
		std::string fragment_shader;

		std::vector<vk::DescriptorSetLayout> descriptor_set_layouts;

		std::function<void(vk::PipelineLayout&,vk::CommandBuffer&, uint32_t this_swapchain_image)> bind_objects;
		std::function<void(uint32_t current_image_index)> update_object_memory;
	};
}
