/*!
 * @file
 *
 * @author Jackson McNeill
 *
 * Vulkan pipeline utilities
 */
#pragma once

#include <vulkan/vulkan.hpp>

#include "vkrender/swapchain.hpp"
#include "vkrender/render_pass.hpp"
#include "vkrender/descriptor.hpp"
#include "vkrender/render_state.hpp"
#include "vkrender/viewport_info.hpp"

namespace vkrender
{

	class pipeline
	{
		public://TODO
		void recreate();
		vk::Device device;
		swapchain const & my_swapchain;
		render_pass const & my_render_pass;
		std::vector<vk::DescriptorSetLayout> my_descriptor_set_layouts;

		std::string vert_path;
		std::string frag_path;
		public:
			viewport_info const & my_viewport_info;
		private:
			state& my_state;	
			event::listener<resolution_change_event::interface_t> resolution_change_listener;
		public:
			pipeline(vk::Device device, swapchain const & my_swapchain, render_pass const & my_render_pass, std::vector<vk::DescriptorSetLayout> my_descriptor_set_layouts, viewport_info const & my_viewport_info, state& my_state, std::string vert_path, std::string frag_path);
			pipeline(pipeline && other) noexcept;

			vk::UniquePipelineLayout pipeline_layout;
			vk::UniquePipeline vk_pipeline;

	};

}
