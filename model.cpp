#include "model.hpp"

/*
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
*/

#include <cstdio>

#include "filesystem/filesystem.hpp"

/*
glm::vec3 convert_to_glm(aiVector3D in)
{
	*!
	 * Negate Y-axis to convert to vulkan coordinate space
	 *
	return glm::vec3{in.x,-in.y,in.z};
}
glm::vec2 convert_to_glm(aiVector2D in)
{
	return glm::vec2{in.x,in.y};
}
*/

namespace vkrender
{	
	
	model::model(std::string path)
	{
		/*Assimp::Importer importer;

		//importer.SetIOHandler( new filesystem::sdl_io_system());

		const aiScene* scene = importer.ReadFile(path,
				aiProcess_CalcTangentSpace | 
				aiProcess_Triangulate | 
				aiProcess_JoinIdenticalVertices | 
				aiProcess_SortByPType |
				aiProcess_GenNormals 

				);

		if (!scene)
		{
			printf("failed %s\n", importer.GetErrorString());
			throw std::runtime_error(importer.GetErrorString());
		}

		printf("num mesh %d\n",scene->mNumMeshes);

		auto mesh = scene->mMeshes[0];


		//naive ripping of the data -- assuming we have triangle meshes 
		for (unsigned int i = 0; i < mesh->mNumFaces; i++)
		{
			indices.push_back(static_cast<uint32_t>(mesh->mFaces[i].mIndices[0]));
			indices.push_back(static_cast<uint32_t>(mesh->mFaces[i].mIndices[1]));
			indices.push_back(static_cast<uint32_t>(mesh->mFaces[i].mIndices[2]));
		}

		//TODO: currently this only supports models with 1 tex cord / vertex
		//assert(mesh->HasTextureCoords(0)); 
		assert(mesh->HasNormals());
		bool mesh_has_colors = (mesh->HasVertexColors(0));
		//TODO this is even worse now
		glm::vec2 tex_cordinates;
		for (unsigned int i = 0; i < mesh->mNumVertices; i++)
		{
			//texturecoords shall always be as big as vertices
			//assuming 1 tex coord per vertex
			//printf("%d this tcord %p %d \n",mesh->mNumVertices, mesh->mTextureCoords[i],i);
			tex_cordinates = convert_to_glm(mesh->mTextureCoords[0][i]);
			aiColor4D color = mesh_has_colors? mesh->mColors[0][i] : aiColor4D{1,1,1,1};
			vertices.push_back(vertex{convert_to_glm(mesh->mVertices[i]),glm::vec3(color.r,color.g,color.b),
					tex_cordinates, convert_to_glm(mesh->mNormals[i])});
		}
		printf("num faces %d\n",mesh->mNumFaces);
		*/
	}
	model::model(std::vector<uint32_t> indices, std::vector<vertex> vertices) :
		indices(indices),
		vertices(vertices)
	{
	}
}
