#include "image.hpp"

#include "vkrender/device_memory.hpp"
#include "vkrender/single_time_commands.hpp"

namespace vkrender
{
	vk::ImageCreateInfo get_create_info(vk::Extent3D extent, uint32_t mip_levels, vk::Format format, vk::ImageTiling tiling, vk::ImageUsageFlags usage)
	{
		vk::ImageCreateInfo create_info = {};
		create_info.imageType = vk::ImageType::e2D;
		create_info.extent = extent;
		create_info.mipLevels = mip_levels;
		create_info.arrayLayers = 1;
		create_info.format = format;
		create_info.tiling = tiling;
		create_info.initialLayout = vk::ImageLayout::eUndefined;
		create_info.usage = usage;
		create_info.samples = vk::SampleCountFlagBits::e1;
		create_info.sharingMode = vk::SharingMode::eExclusive;
		return create_info;
	}
	vk::MemoryAllocateInfo get_memory_info(vk::PhysicalDevice p_device, vk::Device device, vk::MemoryPropertyFlags memory_properties, const vk::UniqueImage& vk_image)
	{
		auto mem_requirements = device.getImageMemoryRequirements(*vk_image);
		return vk::MemoryAllocateInfo{
				mem_requirements.size,
				find_memory_type(p_device.getMemoryProperties(),mem_requirements.memoryTypeBits,memory_properties)
		};
	}
	image::image(vk::PhysicalDevice p_device, vk::Device device, uint32_t width, uint32_t height,uint32_t mip_levels, vk::Format format, vk::ImageTiling tiling, vk::ImageUsageFlags usage, vk::MemoryPropertyFlags memory_properties ) : 
		p_device(p_device),
		device(device),
		format(format), 
		extent(width,height,1),
		mip_levels(mip_levels),
		vk_image(device.createImageUnique(get_create_info(extent,mip_levels,format,tiling,usage))),
		vk_memory(device.allocateMemoryUnique(get_memory_info(p_device,device,memory_properties,vk_image)))
	{
		device.bindImageMemory(*vk_image,*vk_memory,0);
	}
	bool has_stencil_format(vk::Format format)
	{
		return format == vk::Format::eD32SfloatS8Uint || format == vk::Format::eD24UnormS8Uint;
	}
	void image::transition_layout(const command_pool& pool, vk::Queue graphics_queue, vk::ImageLayout new_layout )
	{
		single_time_vulkan_command(device, pool,graphics_queue, [this, &pool,&graphics_queue,&new_layout](vk::CommandBuffer buffer) 
		{
			vk::ImageMemoryBarrier barrier = {};
			barrier.oldLayout = this->layout;
			barrier.newLayout = new_layout;
			barrier.image = *this->vk_image;
			barrier.subresourceRange.baseMipLevel = 0;
			barrier.subresourceRange.levelCount = this->mip_levels;
			barrier.subresourceRange.baseArrayLayer = 0;
			barrier.subresourceRange.layerCount = 1;

			vk::PipelineStageFlags source_stage;
			vk::PipelineStageFlags destination_stage;

			if (new_layout == vk::ImageLayout::eDepthStencilAttachmentOptimal)
			{
				barrier.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eDepth;
				if (has_stencil_format(this->format))
				{
					barrier.subresourceRange.aspectMask |= vk::ImageAspectFlagBits::eStencil;
				}
			}
			else
			{
				barrier.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
			}

			if (this->layout == vk::ImageLayout::eUndefined && new_layout == vk::ImageLayout::eTransferDstOptimal)
			{
				barrier.dstAccessMask = vk::AccessFlagBits::eTransferWrite;

				source_stage = vk::PipelineStageFlagBits::eTopOfPipe;
				destination_stage = vk::PipelineStageFlagBits::eTransfer;
			}
			else if (this->layout == vk::ImageLayout::eTransferDstOptimal && new_layout == vk::ImageLayout::eShaderReadOnlyOptimal)
			{
				barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
				barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;
			
				source_stage = vk::PipelineStageFlagBits::eTransfer;
				destination_stage = vk::PipelineStageFlagBits::eFragmentShader;
			}
			else if (this->layout == vk::ImageLayout::eUndefined && new_layout == vk::ImageLayout::eDepthStencilAttachmentOptimal)
			{
				barrier.dstAccessMask = vk::AccessFlagBits::eDepthStencilAttachmentRead | vk::AccessFlagBits::eDepthStencilAttachmentWrite;

				source_stage = vk::PipelineStageFlagBits::eTopOfPipe;
				destination_stage = vk::PipelineStageFlagBits::eEarlyFragmentTests;
			}
			else
			{
				throw std::runtime_error("unsupported image layout transition");
			}

			buffer.pipelineBarrier(
				source_stage,destination_stage,
				vk::DependencyFlags(),
				0, nullptr,
				0, nullptr,
				1, &barrier
			);

			this->layout = new_layout;
		});
	}
	void image::copy_from_buffer(const command_pool& pool, vk::Queue graphics_queue,vk::Buffer buffer)
	{
		single_time_vulkan_command(device,pool,graphics_queue,[this,buffer](vk::CommandBuffer cmd_buffer)
		{
			vk::BufferImageCopy reigon = {};
			reigon.imageSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
			reigon.imageSubresource.mipLevel = 0;
			reigon.imageSubresource.baseArrayLayer = 0;
			reigon.imageSubresource.layerCount = 1;

			reigon.imageExtent = this->extent;

			cmd_buffer.copyBufferToImage(buffer,*this->vk_image,vk::ImageLayout::eTransferDstOptimal,{reigon});
		});
	}
	void image::generate_mipmaps(const command_pool& pool, vk::Queue graphics_queue)
	{
		single_time_vulkan_command(device,pool,graphics_queue, [this](vk::CommandBuffer cmd_buffer)
		{
			vk::FormatProperties format_properties = this->p_device.getFormatProperties(this->format);
			if (!(format_properties.optimalTilingFeatures & vk::FormatFeatureFlagBits::eSampledImageFilterLinear))
			{
				throw std::runtime_error("Image format doesn't support linear bliting");
			}
			vk::ImageMemoryBarrier barrier = {};
			barrier.image = *this->vk_image;
			barrier.subresourceRange.aspectMask  = vk::ImageAspectFlagBits::eColor;
			barrier.subresourceRange.baseArrayLayer  = 0;
			barrier.subresourceRange.layerCount  = 1;
			barrier.subresourceRange.levelCount  = 1;

			int32_t mip_width = static_cast<int32_t>(this->extent.width);
			int32_t mip_height = static_cast<int32_t>(this->extent.height);

			for (uint32_t i=1; i < this->mip_levels; i++)
			{
				barrier.subresourceRange.baseMipLevel = i-1;	
				barrier.oldLayout = this->layout;//vk::ImageLayout::eTransferDstOptimal;
				barrier.newLayout = vk::ImageLayout::eTransferSrcOptimal;
				barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
				barrier.dstAccessMask = vk::AccessFlagBits::eTransferRead;

				cmd_buffer.pipelineBarrier(
					vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eTransfer,
					vk::DependencyFlags(),
					0, nullptr,
					0, nullptr,
					1, &barrier
				);

				vk::ImageBlit blit = {};
				blit.srcOffsets[0] = vk::Offset3D{0,0,0};
				blit.srcOffsets[1] = vk::Offset3D{mip_width,mip_height,1};
				blit.srcSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
				blit.srcSubresource.mipLevel = i-1;
				blit.srcSubresource.baseArrayLayer = 0;
				blit.srcSubresource.layerCount = 1;
				blit.dstOffsets[0] = vk::Offset3D{0,0,0};
				blit.dstOffsets[1] = vk::Offset3D{mip_width > 1? mip_width/2 : 1,mip_height > 1? mip_height/2 : 1,1};
				blit.dstSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
				blit.dstSubresource.mipLevel = i;
				blit.dstSubresource.baseArrayLayer = 0;
				blit.dstSubresource.layerCount = 1;

				/*cmd_buffer.blitImage(*this->vk_image, vk::ImageLayout::eTransferSrcOptimal,
					*this->vk_image, this->layout,//vk::ImageLayout::eTransferDstOptimal,
					1, &blit,
					vk::Filter::eLinear
				);*/
				cmd_buffer.blitImage(*this->vk_image, vk::ImageLayout::eTransferSrcOptimal,
					*this->vk_image, vk::ImageLayout::eTransferDstOptimal,
					1, &blit,
					//vk::Filter::eLinear
					vk::Filter::eLinear
				);

				barrier.oldLayout = vk::ImageLayout::eTransferSrcOptimal;
				barrier.newLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
				barrier.srcAccessMask = vk::AccessFlagBits::eTransferRead;
				barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

				cmd_buffer.pipelineBarrier( vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader,
					vk::DependencyFlags(),
					0, nullptr,
					0, nullptr,
					1, &barrier
				);

				if (mip_width>1) mip_width/=2;	
				if (mip_height>1) mip_height/=2;	
			}
			barrier.subresourceRange.baseMipLevel = mip_levels-1;
			barrier.oldLayout = vk::ImageLayout::eTransferDstOptimal;
			barrier.newLayout = vk::ImageLayout::eShaderReadOnlyOptimal; this->layout = vk::ImageLayout::eShaderReadOnlyOptimal;
			barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
			barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

			cmd_buffer.pipelineBarrier( vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader,
				vk::DependencyFlags(),
				0, nullptr,
				0, nullptr,
				1, &barrier
			);

		});
	}
	void image::generate_image_view(vk::ImageAspectFlags aspect_flags)
	{
		image_view = {device.createImageViewUnique(vk::ImageViewCreateInfo(vk::ImageViewCreateFlags(),*vk_image,vk::ImageViewType::e2D,format,vk::ComponentMapping(),vk::ImageSubresourceRange(aspect_flags,0,mip_levels,0,1)))};
	}
}
