/*!
 * Viewport info that can automatically update with aspect ratio
 */
#pragma once

#include "vkrender/swapchain.hpp"

namespace vkrender
{
	//TODO multi-viewport support
	class viewport_info
	{
		public:
			virtual vk::Extent2D get_extent() const = 0;
			virtual vk::Offset2D get_offset() const = 0;

			virtual ~viewport_info() = default;
	};
}
