/*!
 * @file
 *
 * @author Jackson McNeill
 */
#pragma once

#include <vulkan/vulkan.hpp>

#include "external/glm.hpp"
#include "vkrender/device_memory.hpp"

namespace vkrender
{
	class uniform_buffer_base {};

	template<typename uniform_buffer_object_t>
	class uniform_buffer : public uniform_buffer_base
	{
		public:
			inline uniform_buffer(vk::PhysicalDevice p_device, vk::Device device) :
				buffer(p_device,device,sizeof(uniform_buffer_object_t), vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent)
			{
			}

			device_buffer<vk_flags_proxy(vk::BufferUsageFlagBits::eUniformBuffer)> buffer;
	};
}
