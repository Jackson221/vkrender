# Render Module

The render module is a raster render using Vulkan. It is still under heavy development.

# Dependancies

Depends on module3D modules: string_manip, filesystem, algorithm, variadic_util, logger

Also depends on glm

# Use

Simply add the files to your build system, and provide the parent directory of all depandancies as an include flag (i.e. -I"src/" )

# License

Project is subject to the GNU AGPL version 3 or any later version, AGPL version 3 being found in LICENSE in the project root directory.
