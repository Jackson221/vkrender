#include "geometry_buffers.hpp"

#include "uniform_buffer.hpp"

namespace vkrender
{

	geometry_buffers::geometry_buffers(const command_pool& pool, const std::vector<vertex>& vertices_cont, const std::vector<uint32_t>& indices_cont) :
		vertex_buffer(pool.p_device,pool.device, vertices_cont.size() * sizeof(vertex), vk::MemoryPropertyFlagBits::eDeviceLocal),
		index_buffer(pool.p_device,pool.device, indices_cont.size() * sizeof(uint32_t), vk::MemoryPropertyFlagBits::eDeviceLocal),
		indices(indices_cont.size())
	{
		update_buffer(pool, vertices_cont);
		update_buffer(pool, indices_cont);
	}


	void geometry_buffers::update_buffer(command_pool const & pool, std::vector<uint32_t> const & indices_cont)
	{
		//TODO safety in terms of the size of the vector
		index_buffer.transfer_c_data_into(use_single_time_command_buffer_with{pool,pool.queues.graphics_queue},indices_cont);
		indices = indices_cont.size();
	}
	void geometry_buffers::update_buffer(command_pool const & pool, std::vector<vertex> const & vertices_cont)
	{
		vertex_buffer.transfer_c_data_into(use_single_time_command_buffer_with{pool,pool.queues.graphics_queue},vertices_cont);
	}
	
}
