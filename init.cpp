#include "init.hpp"

#include <optional>
#include <type_traits>
#include <memory>
#include <vulkan/vulkan.hpp>

#include "SDL.h"
#include "SDL_vulkan.h"

#include "filesystem/filesystem.hpp"
#include "logger/logger.hpp"
#include "vkrender/descriptor.hpp"

#include "vkrender/object.hpp"




constexpr char debug_callback_msg[] = "Validation layer: %s%\n";
VKAPI_ATTR VkBool32 VKAPI_CALL debug_callback(
	VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
	 VkDebugUtilsMessageTypeFlagsEXT messageType,
	 const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
	 void* pUserData)
{

    std::cerr << "validation layer: " << pCallbackData->pMessage  << std::endl;

	logger::error_log<debug_callback_msg>(pCallbackData->pMessage);

    return VK_FALSE;
}



/*
VKAPI_ATTR VkResult VKAPI_CALL vkCreateDebugReportCallbackEXT(
	VkInstance instance,
	const VkDebugReportCallbackCreateInfoEXT* pCreateInfo,
	const VkAllocationCallbacks* pAllocator,
	VkDebugReportCallbackEXT* pCallback
)
{
	return vkrender::CreateDebugReportCallbackEXT( instance, pCreateInfo, pAllocator, pCallback );
}
*/


namespace vkrender
{
	bool enable_validation_layers = 
#ifdef DEBUG
		true;
#else
		false;
#endif
	
	std::vector<const char*> get_required_extensions(SDL_Window* window)
	{
		std::vector<const char*> output;
		unsigned int count; 
		SDL_Vulkan_GetInstanceExtensions(window,&count,nullptr);
		
		output.resize(count);
		SDL_Vulkan_GetInstanceExtensions(window,&count,output.data());

		if (enable_validation_layers)
		{
			output.push_back("VK_EXT_debug_utils");
		}
		return output;
	}

	std::vector<const char*> default_validation_layers = 
	{
		"VK_LAYER_KHRONOS_validation",
	};
	auto blank_validation_layers = std::vector<const char*>{};

	constexpr char layer_present_msg[] = "Device has validation layer available: %s%";
	void throw_if_validation_layers_not_available(std::vector<const char*> layers_to_check)
	{
		auto supported_properties = vk::enumerateInstanceLayerProperties();
		for (auto availablelayer_properties : supported_properties)
		{
			logger::console_log<layer_present_msg>(availablelayer_properties.layerName);
		}

		for (auto checklayer_properties : layers_to_check)
		{
			bool found_layer = false;
			for (auto availablelayer_properties : supported_properties)
			{
				if (strcmp(checklayer_properties,availablelayer_properties.layerName))
				{
					found_layer = true;
					break;
				}
			}
			if (!found_layer)
			{
				throw std::runtime_error(std::string("Couldn't find validation layer: ")+std::string(checklayer_properties));
			}
		}
	}

	const std::vector<const char*> device_extensions = 
	{
		VK_KHR_SWAPCHAIN_EXTENSION_NAME,
	};

	constexpr char unsupported_extension_msg[] = "Graphics card doesn't support extensions:";
	constexpr char unsupported_extension_specific_msg[] = "%s%";

	constexpr char queue_family_incomplete_msg[] = "Queue family incomplete on graphics card";

	constexpr char swapchain_no_formats_or_present_modes_msg[] = "Graphics card lacking any swapchain format or present mode";

	constexpr char device_no_sampler_anistropy_msg[] = "Graphics card lacking sampler anistropy";
	bool is_device_suitable(vk::PhysicalDevice device, vk::SurfaceKHR surface, std::vector<const char*> device_extensions)
	{
		auto unsupported_extensions = get_device_unsupported_extensions(device,device_extensions);
		if (!unsupported_extensions.empty())
		{
			logger::console_log<unsupported_extension_msg>();
			for (auto extension : unsupported_extensions)
			{
				logger::console_log<unsupported_extension_specific_msg>(extension);
			}	
			//return false;
		}
		auto indices = queue_family_indices(device,surface);
		if (!indices.is_complete())
		{
			logger::console_log<queue_family_incomplete_msg>();
			//return false;
		}
		auto info = device_swapchain_info(device,surface);
		if (info.formats.empty() || info.present_modes.empty())
		{
			logger::console_log<swapchain_no_formats_or_present_modes_msg>();
			//return false;
		}
		auto features = device.getFeatures();
		if (!features.samplerAnisotropy)
		{
			logger::console_log<device_no_sampler_anistropy_msg>();
			//return false;
		}
		return true;
	}
	vk::SurfaceKHR create_surface(vk::Instance inst, SDL_Window* window)
	{
		VkSurfaceKHR new_surface;
		if (!SDL_Vulkan_CreateSurface(window,static_cast<VkInstance>(inst),&new_surface))
		{
			auto error = std::string("Couldn't create window surface, SDL says: ")+std::string(SDL_GetError());
			throw std::runtime_error(error);
		}
		return static_cast<vk::SurfaceKHR>(new_surface); 
	}


	constexpr char p_device_msg[] = "Graphics card %s% selected";
	constexpr char p_device_failed_to_find_try_anyway_msg[] = "WARNING - Your graphics cards are not suitable. Trying one anyway...";
	vk::PhysicalDevice choose_device(vk::Instance inst, vk::SurfaceKHR surface)
	{
		std::vector<vk::PhysicalDevice> devices = inst.enumeratePhysicalDevices();

		
		vk::PhysicalDevice p_device;
		bool found_device = false;
		for (vk::PhysicalDevice& maybe_device : devices)
		{
			if (is_device_suitable(maybe_device,surface,device_extensions))
			{
				p_device = maybe_device;
				found_device = true;
				break;
			}
		}
		if (!found_device) 
		{
			if (devices.size() >= 1) //try one anyway
			{
				found_device = true;
				p_device = devices[0];
				logger::console_log<p_device_failed_to_find_try_anyway_msg>();
			}
			else
			{
				throw std::runtime_error("No suitable Vulkan devices");
			}
		}
		logger::console_log<p_device_msg>(p_device.getProperties().deviceName);
		return p_device;
	}



	//vk::DebugUtilsMessengerEXT debug_callback_instance; //TODO: move to renderer class
	std::tuple<vk::DispatchLoaderDynamic, vk::DebugUtilsMessengerEXT> create_dld_and_enable_validation_layers(vk::Instance inst, vk::Device device, bool enable_validation_layers)
	{
		//auto dld = vk::DispatchLoaderDynamic<vk::DynamicLoader>(inst,device);
		vk::DispatchLoaderDynamic dld{};//inst);
		vk::DebugUtilsMessengerEXT debug_callback_instance; 
		dld.init(inst,device);

		if (enable_validation_layers)
		{
			throw_if_validation_layers_not_available(default_validation_layers);

			vk::DebugUtilsMessengerCreateInfoEXT debugInfo;
			debugInfo.messageSeverity = vk::DebugUtilsMessageSeverityFlagBitsEXT::eError | vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning;
			debugInfo.messageType = vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral |
									vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance |
									vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation;
			debugInfo.pfnUserCallback = reinterpret_cast<PFN_vkDebugUtilsMessengerCallbackEXT>(debug_callback);
			debug_callback_instance = inst.createDebugUtilsMessengerEXT(debugInfo,nullptr,dld);
		}
		return std::make_tuple(dld, debug_callback_instance);
	}
	vk::UniqueDevice find_needed_features_and_create_device(vk::PhysicalDevice p_device, vk::SurfaceKHR surface, bool enable_validation_layers)
	{
		auto features = vk::PhysicalDeviceFeatures{};
		features.samplerAnisotropy = true;
		return create_device(p_device,surface,features,(enable_validation_layers? default_validation_layers : blank_validation_layers ),device_extensions );
	}
	vk::UniqueInstance create_instance(SDL_Window* window, std::string name, uint32_t version)
	{
		auto extensions = get_required_extensions(window);

		const vk::ApplicationInfo app_info = {name.c_str(),version,"Module3D",VK_API_VERSION_1_1};		

		if (enable_validation_layers)
		{
			throw_if_validation_layers_not_available(default_validation_layers);
		}
		auto maybe_validation_layers = enable_validation_layers? default_validation_layers : blank_validation_layers;

		const vk::InstanceCreateInfo instance_info = {vk::InstanceCreateFlags(),&app_info,static_cast<uint32_t>(maybe_validation_layers.size()), maybe_validation_layers.data(),static_cast<uint32_t>(extensions.size()),extensions.data()};
		
		return vk::createInstanceUnique(instance_info,nullptr);
	}

	descriptor_pool create_descriptor_pool(vk::Device device, size_t num_objects, const size_t num_swapchain_images)
	{
		std::vector<std::vector<vk::DescriptorPoolSize>> pool_sizes;
		for (size_t i = 0; i < num_objects; i++)
		{
			for (size_t o = 0; o < num_swapchain_images; o++)
			{
				pool_sizes.push_back(object::my_descriptor_layout_bindings.get_pool_size_needed());
			}
		}
		descriptor_pool my_pool(device,pool_sizes,num_swapchain_images*num_objects);
		return my_pool;
	}

	//TODO this needs to be in a diferent module
	int get_display_width()
	{
		SDL_Rect rect;
		SDL_GetDisplayBounds(0,&rect);
		return rect.w;
	}
	int get_display_height()
	{
		SDL_Rect rect;
		SDL_GetDisplayBounds(0,&rect);
		return rect.h;
	}
	vkrender::resolution_t get_window_size(SDL_Window* window)
	{
		int width,height;
		SDL_GetWindowSize(window,&width,&height);
		return {static_cast<uint32_t>(width),static_cast<uint32_t>(height)};
	}

	class vr_viewport_info : public viewport_info
	{
		swapchain const & my_swapchain;
		bool is_right_eye;
		bool is_right_to_left;
		public:
			vr_viewport_info(swapchain const & my_swapchain, bool is_right_eye, bool is_right_to_left) :
				my_swapchain(my_swapchain),
				is_right_eye(is_right_eye),
				is_right_to_left(is_right_to_left)
			{
			}
			vk::Extent2D get_extent() const override
			{
				return {
					is_right_to_left? my_swapchain.image_extent.width/2 : my_swapchain.image_extent.width,
					is_right_to_left? my_swapchain.image_extent.height : my_swapchain.image_extent.height/2,
				};
			}
			vk::Offset2D get_offset() const override
			{
				return {
					static_cast<int32_t>(is_right_to_left? (is_right_eye? my_swapchain.image_extent.width/2 : 0) : 0),
					static_cast<int32_t>(is_right_to_left? 0 : (is_right_eye? my_swapchain.image_extent.height/2 : 0))
				};
			}
			~vr_viewport_info() override
			{
			}

	};

	class viewport_info_2d : public viewport_info
	{
		swapchain const & my_swapchain;
		public:
			viewport_info_2d(swapchain const & my_swapchain) :
				my_swapchain(my_swapchain)
			{
			}
			vk::Extent2D get_extent() const override
			{
				return {
					my_swapchain.image_extent.width,
					my_swapchain.image_extent.height
				};
			}
			vk::Offset2D get_offset() const override
			{
				return {
					0,
					0
				};
			}
			~viewport_info_2d() override
			{
			}
	};


	void renderer::recreate_command_buffers()
	{
		my_command_buffers.clear();
		create_command_buffers();
	}
	void renderer::create_command_buffers()
	{
		for (size_t i = 0;i<num_swapchain_images;i++)
		{
			std::vector<full_object_pipeline*> these_pipelines;
			for (auto & [id,pipe] : my_pipelines.map)
			{
				these_pipelines.push_back(&pipe);
			}

			my_command_buffers.emplace_back(*device,my_command_pool,my_render_pass,my_framebuffers[i],*my_viewport_info,these_pipelines,i,my_state);
		}
	}

	uint32_t to_sdl_fullscreen_flags(fullscreen_mode_t fullscreen)
	{
		switch(fullscreen)
		{
			case fullscreen_mode_t::windowed:
				return 0;
			case fullscreen_mode_t::fullscreen_desktop:
				return SDL_WINDOW_FULLSCREEN_DESKTOP;
			case fullscreen_mode_t::fullscreen:
				return SDL_WINDOW_FULLSCREEN;
		}
	}


	glm::vec2 get_res_to_create()
	{
		SDL_DisplayMode mydpm;
		SDL_GetCurrentDisplayMode(0,&mydpm);
		return {mydpm.w,mydpm.h};
	}

	renderer::renderer(glm::vec2 resolution, res_mode_t res_mode, fullscreen_mode_t fullscreen, std::string name, uint32_t version) :
		is_fullscreen(fullscreen),
		//Christ, we are supporting Guessed Fullscreen, Defaulted on Guess Windowed Mode (to SDres), Guessed Fullscreen Desktop (multi-monitor), and then all that for specified as well.
		//This is complicated.
		window(SDL_CreateWindow(name.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, res_mode == res_mode_t::specified? resolution.x : (fullscreen == fullscreen_mode_t::windowed)? 1280 : (res_mode == res_mode_t::guess)? get_res_to_create().x : 0, res_mode == res_mode_t::specified? resolution.y : (fullscreen == fullscreen_mode_t::windowed)? 800 : (res_mode == res_mode_t::guess)? get_res_to_create().y : 0, SDL_WINDOW_RESIZABLE | SDL_WINDOW_VULKAN | to_sdl_fullscreen_flags(fullscreen))),
		my_state(get_window_size(window),true,true,true),
		inst(create_instance(window,name,version)),
		surface( create_surface(*inst,window),*inst),
		p_device(choose_device(*inst, *surface)),
		device(find_needed_features_and_create_device(p_device,*surface,enable_validation_layers)),
		my_swapchain(p_device,*device,*surface,window,my_state),
		num_swapchain_images(my_swapchain.swapchain_images.size()),
		my_viewport_info(std::make_unique<viewport_info_2d>(my_swapchain)),
		my_render_pass(p_device,*device,my_swapchain,my_state),
		my_command_pool(p_device,*device,*surface),
		my_pipelines(next_pipeline_id),
		//the Helper paradign, when you realize that initialization lists can run code too! :)
		helper([this]()
		{
			/*pipeline_object_ids.resize(pipelines.size());
			for (size_t i = 0; i < pipelines.size(); i++)
			{
				auto descriptor_set_layouts = std::vector{object_descriptor_layout.get()};

				my_pipelines.emplace_back(*device,my_swapchain,my_render_pass, descriptor_set_layouts,*my_viewport_info,my_state,
							filesystem::absolute_path(pipelines[i].vertex_shader),filesystem::absolute_path(pipelines[i].fragment_shader));
			}*/

			my_framebuffers.reserve(num_swapchain_images);
			for (size_t i =0;i<num_swapchain_images;i++)
			{
				my_framebuffers.emplace_back(p_device,*device,my_swapchain,my_render_pass,i,my_state);
			}

			create_command_buffers();
		}),
		my_frame_drawer(p_device,*device,my_swapchain,my_framebuffers[0],my_command_buffers)
	{
		printf("res_mode_t i: %d\n",static_cast<int>(res_mode));
#ifdef PLATFORM_TYPE_POSIX
#ifdef DEBUG
		//debugging only supported on posix platforms
		std::tie(dld,debug_callback) = create_dld_and_enable_validation_layers(*inst,*device,enable_validation_layers);
#endif
#endif
		for (uint32_t i = 0; i < num_swapchain_images; i++)
		{
			my_command_buffers[i].start();
			my_command_buffers[i].end({});
		}
	}

	void renderer::change_resolution(glm::vec2 resolution, res_mode_t res_mode, fullscreen_mode_t fullscreen)
	{
		//basically, if the screen is currently fullscreen, first change the fullscreen-ness then resolution.
		//If it's currently windowed, first change the resolution, then fullscreen-ness
		//This avoids causing the user's monitor to re-adjust resolution when not neccessary.
		if (fullscreen != fullscreen_mode_t::windowed) //(is_fullscreen is whether the screen WAS fullscreen, but not neccessarily whether it will be after the change) 
		{
			SDL_SetWindowFullscreen(window,to_sdl_fullscreen_flags(fullscreen));
		}
		SDL_RestoreWindow(window);
		SDL_SetWindowSize(window,static_cast<int>(resolution.x),static_cast<int>(resolution.y));
		if (is_fullscreen == fullscreen_mode_t::windowed)
		{
			SDL_SetWindowFullscreen(window,to_sdl_fullscreen_flags(fullscreen));
		}

		is_fullscreen = fullscreen;
	}
	glm::vec2 renderer::get_resolution()
	{
		int x,y;
		SDL_GetWindowSize(window,&x,&y);
		return {x,y};
	}


	void renderer::refresh_objects()
	{
		std::vector<full_object_pipeline*> these_pipelines;
		for (auto & [id,pipe] : my_pipelines.map)
		{
			these_pipelines.push_back(&pipe);
		}

		my_command_buffers[my_swapchain.current_image_index].start();
		my_command_buffers[my_swapchain.current_image_index].end(these_pipelines);

	}
	renderer::~renderer() noexcept
	{
		printf("rest in spahgetti never forgetti\n");
		device->waitIdle();

#ifdef PLATFORM_TYPE_POSIX
#ifdef DEBUG
		inst->destroyDebugUtilsMessengerEXT(debug_callback.value(),nullptr,dld.value());
#endif 
#endif
	}
	void renderer::start_frame()
	{
		my_frame_drawer.advance_frame();
	}
	void renderer::end_frame()
	{
		for (auto& [id,pipeline] : my_pipelines.map)
		{
			pipeline.descriptor.update_object_memory(my_swapchain.current_image_index);
		}
		my_frame_drawer.draw();
	}
	camera::camera& renderer::get_camera()
	{
		return my_frame_drawer.camera;
	}

	algorithm::ecs_object<full_object_pipeline> renderer::create_pipeline(full_object_pipeline_descriptor pipeline_descriptor)
	{
		size_t id = my_pipelines.push_back(full_object_pipeline{pipeline_descriptor, pipeline(*device,my_swapchain,my_render_pass, pipeline_descriptor.descriptor_set_layouts,*my_viewport_info,my_state,
					filesystem::absolute_path(pipeline_descriptor.vertex_shader),filesystem::absolute_path(pipeline_descriptor.fragment_shader))});
		return {my_pipelines,id};
	}
}
