#include "command_pool.hpp"

namespace vkrender
{
	command_pool::command_pool(vk::PhysicalDevice p_device, vk::Device device, vk::SurfaceKHR surface) : 
		device(device),
		p_device(p_device),
		indices(p_device,surface),
		queues(device,indices),
		vk_command_pool(device.createCommandPoolUnique({vk::CommandPoolCreateFlagBits::eResetCommandBuffer,indices.graphics_family.value()}))
	{
	}
}
