/*!
 * Abstracts vulkan descriptors
 */
#pragma once

#include <ctime>
#include <math.h>
#include <type_traits>
#include <vector>
#include <list>
#include <algorithm>
#include <map>

#include <vulkan/vulkan.hpp>

#include "vkrender/device_memory.hpp"
#include "vkrender/texture.hpp"
#include "algorithm/container.hpp"
#include "uniform_buffer.hpp"

#include "variadic_util/variadic_util.hpp"


namespace vkrender
{
	class descriptor_pool
	{
		vk::Device device;
		inline vk::UniqueDescriptorPool _init(vk::Device device, std::vector<std::vector<vk::DescriptorPoolSize>> all_set_sizes, uint32_t sets)
		{
			//Basically: Take every DescriptorPoolSize from all_set_sizes, and if they are of the same type (e.x. both describing a texture), add their sizes together.
			auto sizes_amalgamated = algorithm::liquidate_vector(all_set_sizes);

			//We "ID" similar elements based on what I just said above -- by what they are describing.
			std::function get_id = [](const vk::DescriptorPoolSize& in) {return in.type;}; 
			//And then perform the sum operation on the two similar elements to result in just one element. So from 2->1
			std::function sum = 
				[](const vk::DescriptorPoolSize& first, const vk::DescriptorPoolSize& second)
				{
					return vk::DescriptorPoolSize{first.type,first.descriptorCount+second.descriptorCount
				};};
			//Which, finally results in the `size` variable being the result of the previous comment
			std::vector<vk::DescriptorPoolSize> sizes = algorithm::combine_similar_elements(sizes_amalgamated,get_id,sum);


			//Do this in expanded style because it doesn't inline cleanly
			vk::DescriptorPoolCreateInfo info = {};
			info.poolSizeCount = static_cast<uint32_t>(sizes.size());
			info.pPoolSizes = sizes.data();
			info.maxSets = sets;
			return device.createDescriptorPoolUnique(info);
		}
		public:
			vk::UniqueDescriptorPool pool;

			/*!
			 * Create a pool to hold several descriptor sets
			 */
			descriptor_pool(vk::Device device, std::vector<std::vector<vk::DescriptorPoolSize>> all_set_sizes, uint32_t sets) :
				device(device),
				pool(_init(device,all_set_sizes,sets))
			{
			}

			//Sanityhook
			descriptor_pool(descriptor_pool && other) = default;
	};

	////
	template<uint32_t _location, typename T, vk::ShaderStageFlagBits _stage>
	struct descriptor_layout_info
	{
		static constexpr uint32_t location = _location;
		static constexpr vk::ShaderStageFlags stage = _stage;

		using type = T;
	};
	template<typename T>
	struct descriptor_info
	{
		uint32_t location;
		T const & object;
	};
	////
	template<uint32_t _location, vk::ShaderStageFlagBits _stage, typename T>
	struct descriptor_info2_t
	{
		static constexpr uint32_t location = _location;
		static constexpr vk::ShaderStageFlags stage = _stage;

		using type = T;
		type const & object;
	};

	template<typename T>
	descriptor_info(uint32_t, T const &) -> descriptor_info<T>;

	template<uint32_t _location, vk::ShaderStageFlagBits _stage, typename T>
	auto descriptor_info2(T const & in)
	{
		return descriptor_info2_t<_location,_stage,T>(in);
	}



	//Cannot be constexpr: Vulkan objects can't be constexpr.
	template<typename descriptor_layout_tuple>
	auto get_layout_bindings()
	{
		constexpr size_t size = variadic_util::variadic_size_v<descriptor_layout_tuple>;
		std::array<vk::DescriptorSetLayoutBinding, size> layout_bindings;
		std::set<uint32_t> used_binding_locations;
		//                                      ^reference above
		variadic_util::for_each_iterator<size>([&]<size_t i>()
		{
			using this_info = variadic_util::variadic_element_t<i,descriptor_layout_tuple>;
			using this_described_type = std::decay_t<std::remove_pointer_t<typename this_info::type>>;

			//check to make sure the location isn't already used
			if (used_binding_locations.find(this_info::location) != used_binding_locations.end())
			{
				throw std::runtime_error("location specified has already been taken up");
			}
			used_binding_locations.insert(this_info::location);

			//describe the layout
			vk::DescriptorType descriptor_type{};
			if constexpr(std::is_base_of_v<uniform_buffer_base,this_described_type>)
			{
				descriptor_type = vk::DescriptorType::eUniformBuffer;
			}
			else if constexpr(std::is_same_v<this_described_type,texture>)
			{
				descriptor_type = vk::DescriptorType::eCombinedImageSampler;
			}
			else
			{
				throw std::runtime_error("object "+std::string(typeid(this_described_type).name())+" not supported on descriptor set");
			}

			layout_bindings[i] = vk::DescriptorSetLayoutBinding
			{
				this_info::location,
				descriptor_type,
				1,
				this_info::stage
			};
		});
		return layout_bindings;
	}

	//Basically, a namespace you instantiate with the layout tuple
	template<typename descriptor_layout_tuple>
	class descriptor_layout_bindings
	{
		public:
			//Unfortunately these Vulkan objects just can't possibly be stored at compile-time. And I believe this is intentional for the API,
			//So they are global variables, instantiated before main.
			static auto inline layout_bindings = get_layout_bindings<descriptor_layout_tuple>();
			
			static vk::UniqueDescriptorSetLayout get_descriptor_layout(vk::Device device)
			{
				return device.createDescriptorSetLayoutUnique(vk::DescriptorSetLayoutCreateInfo
				{ 
					vk::DescriptorSetLayoutCreateFlags(),
					layout_bindings.size(),
					layout_bindings.data() 
				});
			}
			static constexpr std::vector<vk::DescriptorPoolSize> get_pool_size_needed()
			{
				std::vector<vk::DescriptorPoolSize> descriptor_size;
				std::map<vk::DescriptorType,size_t> descriptor_size_location;

				for (auto info : layout_bindings)
				{
					auto my_pool_size_location = descriptor_size_location.find(info.descriptorType);
					if (my_pool_size_location != descriptor_size_location.end())
					{
						descriptor_size[my_pool_size_location->second].descriptorCount++;
					}
					else
					{
						descriptor_size_location[info.descriptorType] = std::distance(descriptor_size.begin(),descriptor_size.end());

						descriptor_size.push_back(vk::DescriptorPoolSize{info.descriptorType, 1});
					}
				}
				return descriptor_size;
			}
			/*
			descriptor_layout(vk::Device device) : vk_descriptor_layout(device.createDescriptorSetLayoutUnique(
				vk::DescriptorSetLayoutCreateInfo{ vk::DescriptorSetLayoutCreateFlags(),layout_bindings.size(), layout_bindings.data() } ))
			{
			}*/
	};

	class descriptor
	{
		public:
			
			//TODO validate locations to be the same

			vk::DescriptorSet descriptor_set;

			inline descriptor(vk::Device device, vk::DescriptorSetLayout const & layout, descriptor_pool const & pool)
			{
				//vulkan only allows us to alloc descriptor sets in containers... so allocate one, then take it off the back() of the container
				descriptor_set = device.allocateDescriptorSets(vk::DescriptorSetAllocateInfo{*pool.pool,1,&layout}).back();
			}

			void update(vk::Device device, auto const & descriptor_object_ptr_tuple) //auto: descriptor_layout_info
			{
				std::vector<vk::DescriptorBufferInfo> buffer_info;
				std::vector<vk::DescriptorImageInfo> image_info;
				std::vector<vk::WriteDescriptorSet> writes;
				
				variadic_util::for_each_element_in_variadic(descriptor_object_ptr_tuple, [&](auto info) //info_t: descriptor_info<...>
				{
					auto const & object = info.object;
					using object_t = std::decay_t<decltype(object)>;
					auto location = info.location;

					vk::DescriptorType descriptor_type{};

					auto new_write = vk::WriteDescriptorSet{};
					new_write.dstBinding = location;
					new_write.dstArrayElement = 0;
					new_write.descriptorCount = 1;
					if constexpr(std::is_base_of_v<uniform_buffer_base, object_t>)
					{
						descriptor_type = vk::DescriptorType::eUniformBuffer;

						buffer_info.push_back(vk::DescriptorBufferInfo{
							object.buffer.buffer.get(),
							0, //TODO: buffers will need to specify in their class if they want an offset
							object.buffer.size
						});

						new_write.pBufferInfo = &buffer_info.back();
					}
					else if constexpr(std::is_same_v<object_t,texture>)
					{
						descriptor_type = vk::DescriptorType::eCombinedImageSampler;
						if (!object.my_image.image_view.has_value())
						{
							throw std::runtime_error("Attempted to insert an image object into a vulkan descriptor set -- but without generating image views.");
						}
						image_info.push_back(vk::DescriptorImageInfo{
							object.sampler.get(),
							object.my_image.image_view->get(),
							object.my_image.layout,
						});

						new_write.pImageInfo = &image_info.back();
					}
					else
					{
						throw std::runtime_error("object "+std::string(typeid(object_t).name())+" not supported on descriptor set");
					}
					new_write.descriptorType = descriptor_type;
					//Writes is not complete, the dstSet will be set upon creating the descriptor set
					writes.push_back(new_write);
				});

				//TODO: add some logic for more than one descriptor in the set

				for (auto& write : writes)
				{
					write.dstSet = descriptor_set;
				}
				device.updateDescriptorSets(writes.size(),writes.data(),0,nullptr);
			}

			descriptor(vk::Device device, vk::DescriptorSetLayout const & layout, descriptor_pool const & pool, auto const & descriptor_object_ptr_tuple) :
				descriptor(device,layout,pool)
			{
				update(device, descriptor_object_ptr_tuple);
			}

	};

}
