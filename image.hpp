/*!
 * @file
 *
 * @author Jackson McNeill
 * Creates a Vulkan image and manages its memory
 */
#pragma once
#include <optional>

#include <vulkan/vulkan.hpp>

#include "algorithm/type_traits.hpp"
#include "vkrender/device_memory.hpp"
#include "vkrender/command_pool.hpp"

namespace vkrender
{
	class image
	{
		vk::PhysicalDevice p_device;
		vk::Device device;
		public:
		
			image(vk::PhysicalDevice p_device, vk::Device device, uint32_t width, uint32_t height,uint32_t mip_levels, vk::Format format, vk::ImageTiling tiling, vk::ImageUsageFlags usage, vk::MemoryPropertyFlags memory_properties);

			void transition_layout(const command_pool& pool, vk::Queue graphics_queue, vk::ImageLayout new_layout);
			void copy_from_buffer(const command_pool& pool, vk::Queue graphics_queue,vk::Buffer buffer);

			/*!
			 * Note: The size must be at least as big as the width*height of the image. This isn't checked for!
			 */
			template<algorithm::not_has_size_member_fn T>
			void transfer_c_data_into(use_single_time_command_buffer_with const & objs, T data, uint32_t size)
			{
				device_buffer<vk_flags_proxy(vk::BufferUsageFlagBits::eTransferSrc)> staging_buffer(p_device, device, size, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
				staging_buffer.transfer_c_data_into(objs,data,size);
				copy_from_buffer(objs.pool,objs.queue,*staging_buffer.buffer);
			}
			template<algorithm::has_size_member_fn T>
			void transfer_c_data_into(use_single_time_command_buffer_with const & objs, T data)
			{
				transfer_c_data_into(objs,data.data(),data.size()*sizeof(typename T::value_type));
			}

			void generate_mipmaps(const command_pool& pool, vk::Queue graphics_queue);
			void generate_image_view(vk::ImageAspectFlags aspect_flags = vk::ImageAspectFlagBits::eColor);


			vk::ImageLayout layout = vk::ImageLayout::eUndefined;
			vk::Format format;
			vk::Extent3D extent;
			uint32_t mip_levels;


			vk::UniqueImage vk_image;
			vk::UniqueDeviceMemory vk_memory;

			std::optional<vk::UniqueImageView> image_view;
			
	};

}
