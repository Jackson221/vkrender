#include "render_pass.hpp"

#include <stdexcept>

#include "vkrender/device_memory.hpp"

namespace vkrender
{
	
	render_pass::render_pass(vk::PhysicalDevice p_device, vk::Device device, swapchain const & my_swapchain, state& my_state) : 
		p_device(p_device),
		device(device),
		my_swapchain(my_swapchain),
		my_state(my_state),
		resolution_change_listener(my_state.resolution_change_interface,[this](resolution_t new_res)
		{
			this->device.waitIdle();
			this->recreate();
		}, {}, resolution_change_event::priority_t::render_pass)
	{
		auto color_attachment = vk::AttachmentDescription(
			vk::AttachmentDescriptionFlags(),
			my_swapchain.format.format,
			vk::SampleCountFlagBits::e1,
			vk::AttachmentLoadOp::eClear,
			vk::AttachmentStoreOp::eStore,
			vk::AttachmentLoadOp::eDontCare,//stencil
			vk::AttachmentStoreOp::eDontCare,
			vk::ImageLayout::eUndefined,
			vk::ImageLayout::ePresentSrcKHR
		);
		auto color_attachment_ref = vk::AttachmentReference(0,vk::ImageLayout::eColorAttachmentOptimal);

		auto dependancy = vk::SubpassDependency();/*
			VK_SUBPASS_EXTERNAL,
			0,
			vk::PipelineStageFlagBits::eColorAttachmentOutput,
			vk::PipelineStageFlagBits::eColorAttachmentOutput,
			vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite,
			vk::AccessFlagBits::eIndirectCommandRead,
			vk::DependencyFlags()
		);*/
		dependancy.srcSubpass = VK_SUBPASS_EXTERNAL;
		//dependancy.dstSubpass = 0;
		dependancy.srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
		//dependancy.srcAccessMask = vk::PipelineStageFlags(); 
		dependancy.dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
		dependancy.dstAccessMask = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;


		auto depth_attachment = vk::AttachmentDescription(
			vk::AttachmentDescriptionFlags(),
			my_swapchain.depth_format,
			vk::SampleCountFlagBits::e1,
			vk::AttachmentLoadOp::eClear,
			vk::AttachmentStoreOp::eDontCare,
			vk::AttachmentLoadOp::eDontCare,
			vk::AttachmentStoreOp::eDontCare,
			vk::ImageLayout::eUndefined,
			vk::ImageLayout::eDepthStencilAttachmentOptimal
		);

		auto depth_attachment_ref = vk::AttachmentReference(1,vk::ImageLayout::eDepthStencilAttachmentOptimal);

		auto subpass = vk::SubpassDescription(
			vk::SubpassDescriptionFlags(),
			vk::PipelineBindPoint::eGraphics,
			0,
			nullptr,
			1,
			&color_attachment_ref,
			nullptr,
			&depth_attachment_ref
		);
		std::array<vk::AttachmentDescription,2> attachments = {color_attachment,depth_attachment};
		this->pass = device.createRenderPassUnique(vk::RenderPassCreateInfo{vk::RenderPassCreateFlags(),attachments.size(),attachments.data(),1,&subpass,1,&dependancy});
	}
	void render_pass::recreate()
	{
		auto _p_device = p_device;
		auto _device = device;
		swapchain const & _my_swapchain = my_swapchain;
		auto & _my_state = my_state;

		this->~render_pass();
		new (this) render_pass(_p_device,_device,_my_swapchain,_my_state);
	}
}
