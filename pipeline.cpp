#include "pipeline.hpp"

#include <vector>

#include "vkrender/shader.hpp"
#include "vkrender/uniform_buffer.hpp"
#include "vkrender/vertex.hpp"
#include "filesystem/filesystem.hpp"


namespace vkrender
{
	pipeline::pipeline(vk::Device device, const swapchain& my_swapchain, const render_pass& my_render_pass, std::vector<vk::DescriptorSetLayout> my_descriptor_set_layouts, viewport_info const & my_viewport_info, state& my_state, std::string vert_path, std::string frag_path) :
		device(device),
		my_swapchain(my_swapchain),
		my_render_pass(my_render_pass),
		my_descriptor_set_layouts(my_descriptor_set_layouts),
		vert_path(vert_path),
		frag_path(frag_path),
		my_viewport_info(my_viewport_info),
		my_state(my_state),
		resolution_change_listener(my_state.resolution_change_interface,[this](resolution_t new_res)
		{
			this->device.waitIdle();
			this->recreate();
		}, {}, resolution_change_event::priority_t::pipeline)
	{
		auto vert_mod = create_shader(device,filesystem::read_all_from_file(vert_path));
		auto frag_mod = create_shader(device,filesystem::read_all_from_file(frag_path));

		vk::PipelineShaderStageCreateInfo vert = {
			vk::PipelineShaderStageCreateFlags(),
			vk::ShaderStageFlagBits::eVertex,
			*vert_mod,
			"main"
		};
		vk::PipelineShaderStageCreateInfo frag = {
			vk::PipelineShaderStageCreateFlags(),
			vk::ShaderStageFlagBits::eFragment,
			*frag_mod,
			"main"
		};
		std::vector<vk::PipelineShaderStageCreateInfo> shaders = {vert,frag};

		auto binding_desc = vertex::get_binding_description();
		auto attribute_desc = vertex::get_attribute_descriptions();



		vk::PipelineVertexInputStateCreateInfo vertex_in_info = {
			vk::PipelineVertexInputStateCreateFlags(),
			1,
			&binding_desc,
			attribute_desc.size(),
			attribute_desc.data(),
		};

		vk::PipelineInputAssemblyStateCreateInfo input_assembly = 
		{
			vk::PipelineInputAssemblyStateCreateFlags(),
			vk::PrimitiveTopology::eTriangleList,
			false,
		};



		vk::Viewport viewport = {
			static_cast<float>(my_viewport_info.get_offset().x),
			static_cast<float>(my_viewport_info.get_offset().y),
			static_cast<float>(my_viewport_info.get_extent().width),
			static_cast<float>(my_viewport_info.get_extent().height),
			0.f,
			1.f
		};
		vk::Rect2D scissor = {
			my_viewport_info.get_offset(),
			my_viewport_info.get_extent()
		};

		vk::PipelineViewportStateCreateInfo viewport_state= {
			vk::PipelineViewportStateCreateFlags(),
			1,
			&viewport,
			1,
			&scissor,
		};

		vk::PipelineRasterizationStateCreateInfo rasterizer = {
			vk::PipelineRasterizationStateCreateFlags(),
			false,
			false,
			vk::PolygonMode::eFill,
			vk::CullModeFlagBits::eBack, 
			vk::FrontFace::eCounterClockwise,
			false,
			0.f,
			0.f,
			0.f,
			1.f,
		};

		vk::PipelineMultisampleStateCreateInfo multisampling = {
			vk::PipelineMultisampleStateCreateFlags(),
			vk::SampleCountFlagBits::e1,
			false,
			1.f,
			nullptr,
			false,
			false,
		};

		vk::PipelineColorBlendAttachmentState color_blend_attachment = {};
		color_blend_attachment.blendEnable = true;
		color_blend_attachment.srcColorBlendFactor = vk::BlendFactor::eSrcAlpha;//vk::BlendFactor::eOne;
		color_blend_attachment.dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha;//vk::BlendFactor::eZero;
		color_blend_attachment.colorBlendOp = vk::BlendOp::eAdd;
		color_blend_attachment.srcAlphaBlendFactor = vk::BlendFactor::eOne;
		color_blend_attachment.dstAlphaBlendFactor = vk::BlendFactor::eZero;
		color_blend_attachment.alphaBlendOp = vk::BlendOp::eAdd;
		color_blend_attachment.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;

		vk::PipelineColorBlendStateCreateInfo color_blend = {};
		color_blend.logicOpEnable = false;
		color_blend.logicOp = vk::LogicOp::eCopy;
		color_blend.attachmentCount = 1;
		color_blend.pAttachments = &color_blend_attachment;
		//color_blend.blendConstants  //auto-init'd to 0s
		
		vk::PipelineLayoutCreateInfo pipeline_layout_info = {
			vk::PipelineLayoutCreateFlags(),
			static_cast<uint32_t>(my_descriptor_set_layouts.size()),
			my_descriptor_set_layouts.data()
		};
		pipeline_layout = device.createPipelineLayoutUnique(pipeline_layout_info);

		vk::PipelineDepthStencilStateCreateInfo depth_stencil = {};
		depth_stencil.depthTestEnable = false;//true; //TODO Customize this!
		depth_stencil.depthWriteEnable = true;
		depth_stencil.depthCompareOp = vk::CompareOp::eLess;  
		depth_stencil.depthBoundsTestEnable = false;
		depth_stencil.stencilTestEnable = false;

		vk::GraphicsPipelineCreateInfo pipeline_info = {};

		pipeline_info.stageCount = shaders.size();
		pipeline_info.pStages = shaders.data();
		pipeline_info.pVertexInputState = &vertex_in_info;
		pipeline_info.pInputAssemblyState = &input_assembly;
		pipeline_info.pViewportState = &viewport_state;
		pipeline_info.pRasterizationState = &rasterizer;
		pipeline_info.pMultisampleState = &multisampling;
		pipeline_info.pColorBlendState = &color_blend;
		pipeline_info.pDepthStencilState = &depth_stencil;
		pipeline_info.layout = *pipeline_layout;
		pipeline_info.renderPass = *my_render_pass.pass;
		pipeline_info.subpass = 0;
		pipeline_info.basePipelineHandle = nullptr;
		pipeline_info.basePipelineIndex = -1;

		vk_pipeline = device.createGraphicsPipelineUnique(nullptr,pipeline_info);
	}
	void pipeline::recreate()
	{
		vk::Device _device = device;
		swapchain const & _my_swapchain = my_swapchain;
		render_pass const & _my_render_pass = my_render_pass;
		std::vector<vk::DescriptorSetLayout> _my_descriptor_set_layouts = my_descriptor_set_layouts;
		viewport_info const & _my_viewport_info = my_viewport_info;
		state& _my_state = my_state;
		auto _vert_path = vert_path;
		auto _frag_path = frag_path;

		this->~pipeline();
		new (this) pipeline(_device,_my_swapchain,_my_render_pass,_my_descriptor_set_layouts,_my_viewport_info,_my_state,_vert_path,_frag_path);
	}
	pipeline::pipeline(pipeline && other) noexcept :
		device(other.device),
		my_swapchain(other.my_swapchain),
		my_render_pass(other.my_render_pass),
		my_descriptor_set_layouts(other.my_descriptor_set_layouts),
		vert_path(other.vert_path),
		frag_path(other.frag_path),
		my_viewport_info(other.my_viewport_info),
		my_state(other.my_state),	
		resolution_change_listener(my_state.resolution_change_interface,[this](resolution_t new_res)
		{
			this->device.waitIdle();
			this->recreate();
		}, {}, resolution_change_event::priority_t::pipeline),
		pipeline_layout(std::move(other.pipeline_layout)),
		vk_pipeline(std::move(other.vk_pipeline))
	{
	}

}
