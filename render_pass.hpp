/*!
 * @file
 *
 * @author Jackson McNeill
 * 
 * Manages a Vulkan render pass
 */
#pragma once

#include <vulkan/vulkan.hpp>

#include "vkrender/swapchain.hpp"
#include "vkrender/render_state.hpp"

namespace vkrender
{

	class render_pass
	{
		vk::PhysicalDevice p_device;
		vk::Device device;
		swapchain const & my_swapchain;
		state& my_state;

		event::listener<resolution_change_event::interface_t> resolution_change_listener;

		public://TODO
		void recreate();
		
		public:
			render_pass(vk::PhysicalDevice p_device, vk::Device device, swapchain const & my_swapchain, state& my_state);

			vk::UniqueRenderPass pass;
	};
}
