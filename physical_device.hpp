/*!
 * @file
 *
 * @author Jackson McNeill
 *
 * vk::PhysicalDevice functions
 */
#pragma once

#include <optional>
#include <set>

#include <vulkan/vulkan.hpp>

namespace vkrender
{
	std::vector<const char*> get_device_unsupported_extensions(vk::PhysicalDevice& device, std::vector<const char*> extensions);
	struct queue_family_indices
	{
		queue_family_indices(vk::PhysicalDevice& device, vk::SurfaceKHR& surface);
		std::optional<uint32_t> graphics_family;
		std::optional<uint32_t> present_family;
		std::optional<uint32_t> compute_family;
		bool is_complete();
		std::set<uint32_t> get_unique_queue_families();
	};
	struct device_swapchain_info
	{
		device_swapchain_info(vk::PhysicalDevice& device, vk::SurfaceKHR& surface);
		vk::SurfaceCapabilitiesKHR capabilities;
		std::vector<vk::SurfaceFormatKHR> formats;
		std::vector<vk::PresentModeKHR> present_modes;
	};
}
