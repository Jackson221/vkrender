#include "single_time_commands.hpp"


namespace vkrender
{
	void single_time_vulkan_command(vk::Device device, const command_pool& my_command_pool,vk::Queue queue, std::function<void(vk::CommandBuffer)> func)
	{
		vk::CommandBuffer cmd_buffer;

		auto alloc_info = vk::CommandBufferAllocateInfo{*my_command_pool.vk_command_pool,vk::CommandBufferLevel::ePrimary,1};
		device.allocateCommandBuffers(&alloc_info,&cmd_buffer);
		cmd_buffer.begin({vk::CommandBufferUsageFlagBits::eOneTimeSubmit});

		func(cmd_buffer);

		cmd_buffer.end();
		vk::SubmitInfo info = {};
		info.commandBufferCount = 1;
		info.pCommandBuffers = &cmd_buffer;
		deprecated_on_jobsys_queue_sync_mutex.lock();
		queue.submit(1,&info,nullptr);
		queue.waitIdle();
		deprecated_on_jobsys_queue_sync_mutex.unlock();
		
		device.freeCommandBuffers(*my_command_pool.vk_command_pool,{cmd_buffer});
	}
}
