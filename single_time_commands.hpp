#pragma once

#include <functional>

#include <vulkan/vulkan.hpp>

#include "vkrender/command_pool.hpp"

namespace vkrender
{
	struct use_single_time_command_buffer_with
	{
		command_pool const & pool; 
		vk::Queue queue;
	};
	void single_time_vulkan_command(vk::Device device, const command_pool& my_command_pool,vk::Queue queue, std::function<void(vk::CommandBuffer)> func);
}
