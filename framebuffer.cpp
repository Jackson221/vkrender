#include "framebuffer.hpp"

#include "vkrender/physical_device.hpp"
#include "vkrender/device_memory.hpp"

namespace vkrender
{
	vk::UniqueFramebuffer framebuffer::_init(vk::PhysicalDevice p_device, vk::Device device, const swapchain& my_swapchain, const render_pass& my_render_pass, size_t this_swapchain_image)
	{
		auto attachments = std::array<vk::ImageView,2>({ 
			my_swapchain.swapchain_image_views[this_swapchain_image].get(),
			**my_swapchain.depth_image.image_view, 
		});
		


		vk::FramebufferCreateInfo info = {};
		info.renderPass = *my_render_pass.pass;
		info.attachmentCount = attachments.size();
		info.pAttachments = attachments.data();
		info.width = my_swapchain.image_extent.width;
		info.height = my_swapchain.image_extent.height;
		info.layers = 1;

		return device.createFramebufferUnique(info);
	}
	framebuffer::framebuffer(vk::PhysicalDevice p_device, vk::Device device, const swapchain& my_swapchain, const render_pass& my_render_pass, size_t this_swapchain_image, state& my_state) : 
		p_device(p_device),
		device(device),
		my_swapchain(my_swapchain),
		my_render_pass(my_render_pass),
		this_swapchain_image(this_swapchain_image),
		my_state(my_state),
		resolution_change_listener(my_state.resolution_change_interface,[this](resolution_t new_res)
		{
			this->recreate();
		}, {}, resolution_change_event::priority_t::framebuffer),
		extent(my_swapchain.image_extent),
		vk_framebuffer(_init(p_device,device,my_swapchain,my_render_pass,this_swapchain_image))
	{
	}
	void framebuffer::recreate()
	{
		vk::PhysicalDevice _p_device = p_device;
		vk::Device _device = device;
		swapchain const & _my_swapchain = my_swapchain;
		render_pass const & _my_render_pass = my_render_pass;
		size_t _this_swapchain_image = this_swapchain_image;
		state& _my_state = my_state;

		
		this->device.waitIdle();
		this->~framebuffer();
		new (this) framebuffer(_p_device,_device,_my_swapchain,_my_render_pass,_this_swapchain_image,_my_state);
	}
}
