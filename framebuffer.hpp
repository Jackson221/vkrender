/*!
 * Vulkan framebuffer utilities
 */
#pragma once

#include <array>

#include <vulkan/vulkan.hpp>

#include "vkrender/image.hpp"
#include "vkrender/swapchain.hpp"
#include "vkrender/render_pass.hpp"
#include "vkrender/render_state.hpp"

namespace vkrender
{

	class framebuffer
	{
		vk::PhysicalDevice p_device;
		vk::Device device;
		swapchain const & my_swapchain;
		render_pass const & my_render_pass;
		size_t this_swapchain_image;
		state& my_state;
		event::listener<resolution_change_event::interface_t> resolution_change_listener;

		void recreate();

		vk::UniqueFramebuffer _init(vk::PhysicalDevice p_device, vk::Device device, const swapchain& my_swapchain, const render_pass& my_render_pass, size_t this_swapchain_image);
		public:
			framebuffer(vk::PhysicalDevice p_device, vk::Device device, const swapchain& my_swapchain, const render_pass& my_render_pass, size_t this_swapchain_image, state& my_state);



			vk::Extent2D extent;
			vk::UniqueFramebuffer vk_framebuffer;

	};

}
