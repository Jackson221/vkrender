/*!
 * Keeps track of various states of the renderer
 *
 * Some examples are: resolution, number of viewports
 *
 * Also uses the event module so that objects can be informed of global state changes.
 */
#pragma once

#include "event/event.hpp"


namespace vkrender
{
	struct resolution_t
	{
		uint32_t width;
		uint32_t height;
	};

	namespace resolution_change_event
	{
		enum class priority_t
		{
			swapchain,
			render_pass,
			pipeline,
			framebuffer,
			command_buffer,
			other,
			SIZE
		};

		using event_data_t = resolution_t;
		using my_configuration_types_t = event::event_configuration_types<event_data_t>;
		struct event_configuration_types
		{
			using manager_t = event::manager<event_configuration_types>;
			using configuration_types_t = my_configuration_types_t;
			using interface_types_tuple_t = std::tuple<event::endpoint_interface<event_configuration_types,std::mutex,priority_t>>;
		};

		using manager_t = event_configuration_types::manager_t;
		using interface_t = event::endpoint_interface<event_configuration_types,std::mutex,priority_t>;
		inline manager_t manager("resolution_change");
	}

	class state
	{
		public:
			resolution_t resolution;

			//resolution_change_event::manager_t resolution_change_manager;
			resolution_change_event::interface_t resolution_change_interface;

			inline void change_resolution(resolution_t new_res)
			{
				resolution = new_res;

				resolution_change_interface.push_event(new_res);
				resolution_change_interface.send_events();
				resolution_change_interface.dispatch_events();
			}

			/*bool fullscreen;
			using fullscreen_event_t = event::type<bool>;
			fullscreen_event_t fullscreen_event;
			inline void change_fullscreen(bool new_fullscreen)
			{
				fullscreen = new_fullscreen;
				fullscreen_event.push_event(new_fullscreen);
				fullscreen_event.dispatch_events();
			}

			bool screen_oriented_x_as_horizontal;
			using screen_orientation_event_t = event::type<bool>;
			screen_orientation_event_t screen_orientation_event;
			inline void change_screen_orientation(bool new_orientation)
			{
				screen_oriented_x_as_horizontal = new_orientation;
				screen_orientation_event.push_event(new_orientation);
				screen_orientation_event.dispatch_events();
			}

			bool stereoscopy_viewports;
			using stereoscopy_event_t = event::type<bool>;
			stereoscopy_event_t stereoscopy_event;
			inline void change_stereoscopy(bool new_stereoscopy)
			{
				stereoscopy_viewports = new_stereoscopy;
				stereoscopy_event.push_event(new_stereoscopy);
				stereoscopy_event.dispatch_events();
			}
			*/

			inline state(resolution_t _resolution, bool _fullscreen, bool _screen_oriented_x_as_horizontal, bool _stereoscopy_viewports) :
				resolution(_resolution),
				//resolution_change_manager("resolution_change_interface"),
				resolution_change_interface(resolution_change_event::manager)/*,
				fullscreen(_fullscreen),
				fullscreen_event("fullscreen_event"),
				screen_oriented_x_as_horizontal(_screen_oriented_x_as_horizontal),
				screen_orientation_event("screen_orientation_event"),
				stereoscopy_viewports(_stereoscopy_viewports),
				stereoscopy_event("stereoscopy_event")*/
			{
			}



	};

}
