/*!
 * @title
 *
 * @author Jackson McNeill
 *
 * Initializaing the Vulkan renderer
 */
#pragma once

#include <unordered_map>
#include <vector>
#include <iostream>
#include <cstring>
#include <memory>

#include "SDL.h"

#include <vulkan/vulkan.h>

#include <vulkan/vulkan.hpp>

#include "vkrender/physical_device.hpp"
#include "vkrender/device.hpp"
#include "vkrender/swapchain.hpp"
#include "vkrender/render_pass.hpp"
#include "vkrender/pipeline.hpp"
#include "vkrender/framebuffer.hpp"
#include "vkrender/command_pool.hpp"
#include "vkrender/texture.hpp"
#include "vkrender/model.hpp"
#include "vkrender/geometry_buffers.hpp"
#include "vkrender/uniform_buffer.hpp"
#include "vkrender/descriptor.hpp"
#include "vkrender/command_buffer.hpp"
#include "vkrender/object.hpp"
#include "vkrender/draw.hpp"
#include "vkrender/render_state.hpp"
#include "vkrender/viewport_info.hpp"
#include "input/computer.hpp"

#include "algorithm/ecs.hpp"






#include "synchro/synchro.hpp"

namespace vkrender
{
	
	extern bool enable_validation_layers;

	bool are_validation_layers_available(std::vector<const char*> layers_to_check);
	
	std::vector<const char*> get_required_extensions();

	descriptor_pool create_descriptor_pool(vk::Device device, size_t num_objects, const size_t num_swapchain_images);

	//TODO move this
	enum class res_mode_t 
	{
		specified,
		guess,
		multimonitor
	};
	enum class fullscreen_mode_t 
	{
		windowed,
		fullscreen_desktop,
		fullscreen
	};
	//static_assert( )//that this is syncable as int8_t
	class renderer
	{
		struct initialize_helper
		{
			inline initialize_helper(auto fn)
			{
				fn();
			}
		};

		fullscreen_mode_t is_fullscreen;

		
		public: //TODO?
		SDL_Window* window;
		state my_state;
		vk::UniqueInstance inst;
		vk::UniqueSurfaceKHR surface;
		vk::PhysicalDevice p_device;
		vk::UniqueDevice device;

		swapchain my_swapchain;
		const size_t num_swapchain_images;
		std::unique_ptr<viewport_info> my_viewport_info;
		render_pass my_render_pass;
		std::vector<framebuffer> my_framebuffers;
		command_pool my_command_pool;

		size_t next_pipeline_id = 0;
		algorithm::ecs_container<full_object_pipeline> my_pipelines;

		std::vector<command_buffer> my_command_buffers;


		[[no_unique_address]] initialize_helper helper;

		frame_drawer my_frame_drawer;
		
		void create_command_buffers();
		void recreate_command_buffers();

		std::optional<vk::DispatchLoaderDynamic> dld;
		std::optional<vk::DebugUtilsMessengerEXT> debug_callback;

		public:

			renderer(glm::vec2 resolution, res_mode_t res_mode, fullscreen_mode_t fullscreen, std::string name, uint32_t version);
			~renderer() noexcept;
			
			void change_resolution(glm::vec2 resolution, res_mode_t res_mode, fullscreen_mode_t fullscreen);
			glm::vec2 get_resolution();

			void refresh_objects();
			void start_frame();
			void end_frame();

			camera::camera& get_camera();

			algorithm::ecs_object<full_object_pipeline> create_pipeline(full_object_pipeline_descriptor pipeline_descriptor);

	};
				


	void init(std::string name,uint32_t version );


	void shutdown();

}
