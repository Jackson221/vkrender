/*!
 * @file
 *
 * @author Jackson McNeill
 * 
 * Amalgamation of a model, its texture, and its vulkan buffers/descriptors, etc.
 */
#pragma once 

#include <vulkan/vulkan.hpp>
#include <vector>
#include <set>
#include <memory>
#include <tuple>

#include "external/glm.hpp"

#include "vkrender/model.hpp"
#include "vkrender/geometry_buffers.hpp"
#include "vkrender/uniform_buffer.hpp"
#include "vkrender/descriptor.hpp"
#include "vkrender/pipeline.hpp"
#include "vkrender/render_pass.hpp"
#include "vkrender/camera/camera.hpp"

#include "algorithm/ecs.hpp"
#include "algorithm/type_traits.hpp"

namespace vkrender
{

	

	//Common operation, to specify that an ecs container is just default constructed
	template<typename member_ptr_t>
	consteval auto get_default_construct_descriptor(member_ptr_t member_ptr)
	{
		return algorithm::ecs_container_descriptor{member_ptr, [](auto&, uint32_t, auto in)
		{
			return algorithm::remove_template_instantiation_t<algorithm::remove_member_pointer_t<member_ptr_t>>{};
		}};
	}
	//Common operation, to specify the uniform buffer descriptor
	template<typename object_components_t>
	consteval auto get_uniform_buffer_descriptor()
	{
		return algorithm::ecs_container_descriptor
		{
			&object_components_t::object_uniform_buffers_for_swapchain_image,
			[](auto& self, uint32_t num_swapchain_images, auto in)
			{
				using retval_t = algorithm::remove_template_instantiation_t<decltype(object_components_t::object_uniform_buffers_for_swapchain_image)>;
				retval_t retval;
				for (uint32_t i = 0; i < num_swapchain_images; i++)
				{
					retval.push_back(algorithm::remove_template_instantiation_t<retval_t>{});
				}
				return retval;
			}
		};
	}

	
//Defining macros deemed an acceptable tragedy for the cause.

#define VKRENDER_OBJECT_DATA_BASE \
		vk::Device device;\
		vk::PhysicalDevice p_device;\
		descriptor_pool const & my_descriptor_pool;\
		vk::DescriptorSetLayout layout;

#define VKRENDER_DEFINE_self_t(object_components_t) \
	using __declval_t = variadic_util::value_proxy \
	</* They always have descriptor sets, so auto-add this constructor*/\
		algorithm::ecs_container_descriptor \
		{ \
			&object_components_t::object_descriptors_for_swapchain_image, \
			[](auto& self, uint32_t num_swapchain_images, object_data in) \
			{ \
				std::vector<descriptor> retval; \
				/*for (uint32_t i = 0; i < num_swapchain_images; i++) \
				{ \
					retval.push_back(descriptor(in.device, in.layout, in.my_descriptor_pool, descriptors(self,in,i))); \
				} we defer this to object_base sadly cause of `descriptors` not being forward declarable*/ \
				return retval;  \
			} \
		} \
	>;\
	using self_t = algorithm::ecs_objects_group<decltype(std::tuple_cat(std::declval<ecs_member_ptrs>(), std::declval<std::tuple<__declval_t>>())),object_data>;

	struct transform
	{
		glm::vec3 position;
		glm::quat rotation;
	};
	struct transform2d
	{
		glm::vec3 position_and_rotation;	
	};
	struct object_uniform_buffer_object
	{
		glm::mat4 model;
		glm::mat4 view;
		glm::mat4 proj;
		glm::vec3 global_lighting_direction;
	};

	using object_uniform_buffer = uniform_buffer<object_uniform_buffer_object>;

	struct object_components
	{
		size_t next_object_id = 0;
		algorithm::ecs_container<transform> object_transforms;
		algorithm::ecs_container<transform2d> object_transforms_2d;
		algorithm::ecs_container<glm::vec2> object_size_2d;
		algorithm::ecs_container<geometry_buffers> object_geometry_buffers;
		
		//Swapchain stuff.
		//This is special somehow, but I'm yet to devise a way of languageifying that.
		algorithm::ecs_container<std::vector<object_uniform_buffer>> object_uniform_buffers_for_swapchain_image;
		//every object components must have this
		algorithm::ecs_container<std::vector<descriptor>> object_descriptors_for_swapchain_image;

		object_components() :
			object_transforms(next_object_id),
			object_transforms_2d(next_object_id),
			object_size_2d(next_object_id),
			object_geometry_buffers(next_object_id),
			object_uniform_buffers_for_swapchain_image(next_object_id),
			object_descriptors_for_swapchain_image(next_object_id)
		{
		}
	};

	/*!
	 * This is an example of how to use this system
	 *
	 * Define the object_data needed to initialize object components
	 * Then define init functions for the used object components
	 * Then define the descriptors
	 *
	 * And put the two macros used in. Put all of this in the same order, same name.
	 */
	struct full_test
	{
		struct object_data 
		{
			VKRENDER_OBJECT_DATA_BASE

			texture const * texture_ptr;
			const command_pool& my_command_pool;
			model const & my_model_ref;
		};
		//Go through this list of object components and 
		//construct each one as such.
		using ecs_member_ptrs = std::tuple
		<
			variadic_util::value_proxy<get_default_construct_descriptor(&object_components::object_transforms)>,
			variadic_util::value_proxy<algorithm::ecs_container_descriptor
			{
				&object_components::object_geometry_buffers,
				[](auto&, uint32_t, object_data in)
				{
					return geometry_buffers(in.p_device,in.device,in.my_command_pool,in.my_model_ref);
				}
			}>,
			variadic_util::value_proxy<get_uniform_buffer_descriptor<object_components>()>
		>;
		VKRENDER_DEFINE_self_t(object_components)

		inline static auto descriptors(self_t& self, object_data in, uint32_t i)
		{
			//TODO: No need to specify binding number, instead we should specify a "variable name" that will be specified in our shader.
			//We then just do a small pre-processing step for the GLSL, so GLSL->our step->compiler->spirV.
			//Maybe just make that the variable name in the shader code, such that they are equal here and in the shader. That makes it nice and readable.
			//We can then make a for_each macro for this to just specify the things like they where merely function arguments, the names of each and all.
			//	Like a macro version of a let statement. 
			//
			//Does this ever return anything other than a make_tuple call? I mean, do we ever use the space above it in these?
			return std::make_tuple
			(
				descriptor_info2<0,vk::ShaderStageFlagBits::eVertex>(self[&object_components::object_uniform_buffers_for_swapchain_image][i]), //(the uniform buffer for this swapchain image)
				descriptor_info2<1,vk::ShaderStageFlagBits::eFragment>(in.texture_ptr)
			);
		}
	};


	template<typename object_descriptor_class>
	class object_base
	{
		public:
			using zeroth_member_ptr = decltype((std::tuple_element_t<0,typename object_descriptor_class::ecs_member_ptrs>::value).member_ptr);
			using object_data_t = typename object_descriptor_class::object_data;

			using object_components_t = algorithm::get_class_of_member_pointer_t<zeroth_member_ptr>;

			using self_t = typename object_descriptor_class::self_t;

			using object_data_data_t = std::tuple<self_t&, uint32_t, object_data_t>;

			self_t self;
			
			using my_descriptor_layout_bindings_t = decltype(object_descriptor_class::descriptors(std::declval<self_t&>(),std::declval<object_data_t>(),0));

			static auto constexpr my_descriptor_layout_bindings = descriptor_layout_bindings<my_descriptor_layout_bindings_t>{};

			inline object_base(vk::PhysicalDevice p_device, vk::Device device,descriptor_pool const & descriptor_pool, vk::DescriptorSetLayout layout, uint32_t num_swapchain_images, object_components_t & components, object_data_t inputs_tuple) : 
				self(components, object_data_data_t(self, num_swapchain_images, inputs_tuple))
			{
				for (uint32_t i = 0; i < num_swapchain_images; i++)
				{
					self[&object_components_t::object_descriptors_for_swapchain_image].push_back
					(
					 	descriptor(device, layout, descriptor_pool, descriptors(self,inputs_tuple,i))
					);
				}
				/*variadic_util::for_each_type_in_variadic<typename object_descriptor_class::ecs_member_ptrs>([&]<typename T>()
				{
					constexpr algorithm::ecs_container_descriptor ct_descriptor = T::value;
					self[ct_descriptor.member_ptr] = ct_descriptor.func(self,num_swapchain_images, inputs_tuple);
				}); performed in ecs */
			}
			object_base(object_base &&) = default;
			
			object_base(object_base const&) = delete;
			auto operator=(object_base&&) = delete;
			auto operator=(object_base const&) = delete;
	};

	using object = object_base<full_test>;
	//using object2d = object_base<&object_components::object_transforms_2d>;

	template<typename object_t>
	struct object_support
	{
		object_components components;
		vk::UniqueDescriptorSetLayout descriptor_layout;

		object_support(vk::Device device) :
			descriptor_layout(object_t::my_descriptor_layout_bindings.get_descriptor_layout(device))
		{
		}

		inline auto get_bind_objects_fn(std::set<size_t>& pipeline_object_ids_ref)
		{
			return [&](vk::PipelineLayout& layout, vk::CommandBuffer& buffer, uint32_t this_swapchain_image)
			{
				const std::vector<vk::DeviceSize> offsets = {0};
				size_t i =0;
				for (size_t object_id : pipeline_object_ids_ref)
				{
					auto & geo_buf = components.object_geometry_buffers[object_id];
					auto & descriptor = components.object_descriptors_for_swapchain_image[object_id];


					buffer.bindVertexBuffers(0,1,&(geo_buf.vertex_buffer.buffer.get()),offsets.data());
					buffer.bindIndexBuffer(geo_buf.index_buffer.buffer.get(),0,vk::IndexType::eUint32);
					buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics,layout,0,1,&descriptor[this_swapchain_image].descriptor_set,0,nullptr);
					buffer.drawIndexed(geo_buf.indices,1,0,0,0); 

					i++;
				}
			};
		}

		inline auto get_update_objects_screen_position_fn(camera::camera& camera, viewport_info& info)
		{
			return [&](uint32_t current_image_index)
			{
				object_uniform_buffer_object ubo{};

				//3D objects

				ubo.proj = glm::perspective(glm::radians(90.0f), static_cast<float>(info.get_extent().width) /  static_cast<float>(info.get_extent().height), 0.001f, 1000.0f);
				//ubo.proj = glm::ortho(0.f,static_cast<float>(info.get_extent().width),static_cast<float>(info.get_extent().height),0.f, 0.001f, 1000.0f);
				//TODO: ^ User-definable clip-space
					
				ubo.view = glm::translate(glm::inverse(glm::mat4(camera.get_angle())),-camera.get_position(0));

				float angle = 0.f;//static_cast<float>(frames) / 360;

				//rotate lighting arround the x z axis
				float x = 23.486000f;
				float z = -x;//-9.670877f;

				ubo.global_lighting_direction = glm::normalize(glm::vec3{(x*cos( angle) - z*sin(angle)),-2.094169f,(x*sin(angle) + z*cos(angle))});

				for (auto [object_id, transform] : components.object_transforms.map)
				{
					//TODO: move this somewhere sane
					/*ubo.model = glm::translate(
							glm::mat4(obj.rotation),
							obj.position
					);*/
					//ubo.model =  glm::translate(glm::mat4(1.f), transform.position) * glm::mat4(transform.rotation);
					ubo.model =  glm::translate(glm::mat4(1.f), transform.position) * glm::mat4(transform.rotation);
					//ubo.view = glm::inverse(glm::translate(glm::mat4(1.f),camera.get_position(0))*glm::inverse(glm::mat4(camera.get_angle())));

					components.object_uniform_buffers_for_swapchain_image[object_id][current_image_index].buffer.transfer_c_data_into(&ubo,sizeof(ubo));
				}

				//2D objects

				//ubo.proj = glm::ortho( 0.0, 1.0, 1.0, 0.0, -1.0,1.0);
				ubo.proj = glm::ortho( 0.0, static_cast<double>(info.get_extent().width), static_cast<double>(info.get_extent().height), 0.0, -1.0,1.0);

				ubo.view = glm::mat4(1.0);

				for (auto [object_id, transform] : components.object_transforms_2d.map)
				{
					auto size = components.object_size_2d[object_id];
					auto halfsize = glm::vec3{size.x/2.0,size.y/2.0,0};
					//TODO: Why is the coordinate system starting from bottom-left when VK specs say it should be top-right?
					/*ubo.model =  glm::translate(glm::rotate
					(
							glm::translate(glm::mat4(1.f), glm::vec3{transform.position_and_rotation.x, static_cast<double>(info.get_extent().height)-transform.position_and_rotation.y,0}
								- halfsize),
							transform.position_and_rotation.z,
							glm::vec3{0,0,1}
					), halfsize);*/
					
					auto position3 = glm::vec3{transform.position_and_rotation.x, static_cast<double>(info.get_extent().height)-transform.position_and_rotation.y,0};
					ubo.model =  glm::translate(glm::rotate
					(
							glm::translate(glm::mat4(1.f),position3),
							transform.position_and_rotation.z,
							glm::vec3{0,0,1}
					),-halfsize);
					components.object_uniform_buffers_for_swapchain_image[object_id][current_image_index].buffer.transfer_c_data_into(&ubo,sizeof(ubo));
				}
			};
		}
	};
}
